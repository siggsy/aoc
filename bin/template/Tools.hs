{-# LANGUAGE BangPatterns #-}

module Tools
  (
    -- IO
    pretty,
    runParser,

    -- Map
    crossAround,
    xAround,
    starAround,
    bounded,
    parseMap,
    showMap,

    -- Misc
    detectCycle,
  )
where

import Data.Array.IArray (Array, listArray)
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.List (intersperse)
import Data.List.Extra (merge)
import Data.Text (pack)
import GHC.Ix (inRange)
import Text.Pretty.Simple (CheckColorTty (..), OutputOptions (..), defaultOutputOptionsNoColor, pPrintOpt)

--  ,----------------------------------------------------------------------------
--  | IO helpers: usefull fun
--  '----------------------------------------------------------------------------

-- | Pretty print the output with good default parameters
pretty :: (Show a) => a -> IO ()
pretty =
  pPrintOpt
    NoCheckColorTty
    defaultOutputOptionsNoColor
      { outputOptionsCompact = True
      }

-- | Run the provided parser @p@ on string @s@ and crash on error
runParser :: Parser a -> String -> IO a
runParser p s = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

--  ,----------------------------------------------------------------------------
--  | Dealing with maps
--  '----------------------------------------------------------------------------

-- | Cross-like shape around a position
--
-- >>> crossAround (1, 1)
-- [(0,1),(1,0),(1,2),(2,1)]
crossAround :: (Int, Int) -> [(Int, Int)]
crossAround (y, x) =
  [ (y - 1, x),
    (y, x - 1),
    (y, x + 1),
    (y + 1, x)
  ]

-- | X-like shape around a position
--
-- >>> xAround (1, 1)
-- [(0,0),(0,2),(2,0),(2,2)]
xAround :: (Int, Int) -> [(Int, Int)]
xAround (y, x) =
  [ (y - 1, x - 1),
    (y - 1, x + 1),
    (y + 1, x - 1),
    (y + 1, x + 1)
  ]

-- | Star-like shape around a position
--
-- >>> starAround (1, 1)
-- [(0,0),(0,1),(0,2),(1,0),(1,2),(2,0),(2,1),(2,2)]
starAround :: (Int, Int) -> [(Int, Int)]
starAround coor = merge (crossAround coor) (xAround coor)

-- | Bounded shapes around position
--
-- >>> bounded ((0,0), (3,3)) starAround (0,0)
-- [(0,1),(1,0),(1,1)]
bounded ::
  -- | Bounds of the shape
  ((Int, Int), (Int, Int)) ->
  -- | Shape function
  ((Int, Int) -> [(Int, Int)]) ->
  -- | Position (coordinate)
  (Int, Int) ->
  -- | Final shape
  [(Int, Int)]
bounded bounds f = filter (inRange bounds) . f

-- | Parse a map where each character represents a position.
-- @f@ is used to convert char to a more usable data structure
--
-- Example of a parseable @s@:
--
-- > #######
-- > #O....#
-- > #..O..#
-- > #..O..#
-- > #######
parseMap :: (Char -> a) -> String -> Array (Int, Int) a
parseMap f !s =
  let ls@(l : _) = lines s
      m = map f . concat $ ls
      x = length l
      y = length ls
   in listArray ((0, 0), (y - 1, x - 1)) m

-- | Shows map based on provided coordinate bounds and lookup function
--
-- >>> showMap ((0, 0), (3, 3)) (const '.')
-- ". . . .\n. . . .\n. . . .\n. . . .\n"
--
-- >>> showMap ((0, 0), (3,3)) (\(y, x) -> if x == y then '#' else '.')
-- "# . . .\n. # . .\n. . # .\n. . . #\n"
showMap :: ((Int, Int), (Int, Int)) -> ((Int, Int) -> Char) -> String
showMap ((sy, sx), (ey, ex)) lookup =
  unlines $ do
    y <- [sy .. ey]
    return . intersperse ' ' $ do
      x <- [sx .. ex]
      return (lookup (y, x))

--  ,----------------------------------------------------------------------------
--  | Misc
--  '----------------------------------------------------------------------------

-- | From a list of states return a tuple of initial states and cycling states
-- This function only works on sequences @[ a, f(a), f(f(a)), ... ]@
detectCycle :: (Eq a) => [a] -> ([a], [a])
detectCycle as = cycle as as
  where
    cycle (x : xs) (_ : y : ys)
      | x == y = start as xs
      | otherwise = cycle xs ys
    cycle _ _ = (as, [])

    start (x : xs) (y : ys)
      | x == y = ([], x : cyc x xs)
      | otherwise = let (xs', ys') = start xs ys in (x : xs', ys')

    cyc x (y : ys)
      | x == y = []
      | otherwise = y : cyc x ys
