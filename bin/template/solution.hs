{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import System.Environment (getArgs)
import qualified Tools as T

--  ,----------------------------------------------------------------------------
--  | Types: types that we will use in the solution (mostly input)
--  '----------------------------------------------------------------------------

type Input = [Int]

--  ,----------------------------------------------------------------------------
--  | Parse: attoparsec / manual parsing
--  '----------------------------------------------------------------------------

parseInput :: String -> IO Input
parseInput = undefined

--  ,----------------------------------------------------------------------------
--  | Main: run the parser and pass it to partA and partB
--  '----------------------------------------------------------------------------

main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn ",--------------------------"
  putStrLn "| Part A:                  "
  putStrLn "'--------------------------"
  partA input
  putStrLn ""

  putStrLn ",--------------------------"
  putStrLn "| Part B:                  "
  putStrLn "'--------------------------"
  partB input

--  ,----------------------------------------------------------------------------
--  | Part A
--  '----------------------------------------------------------------------------

partA :: Input -> IO ()
partA input = do
  putStrLn "TODO"

--  ,----------------------------------------------------------------------------
--  | Part B
--  '----------------------------------------------------------------------------

partB :: Input -> IO ()
partB input = do
  putStrLn "TODO"
