#!/usr/bin/bash
set -e

abs_path=$(readlink -f "$0")
ROOT=$(dirname "$abs_path")
readonly ROOT

echo -e "+----------------------------+"
echo -e "| * * * Advent Of Code * * * |"
echo -e "+----------------------------+"

day=${1:-$(date +%-d)}
year=${2:-$(date +%Y)}
echo "Creating for $day/12/$year"

# Variables
if [ -z "$AOC_COOKIE" ]; then
    echo "Missing AOC cookie (env AOC_COOKIE)"
    exit 1
fi
aoc_cookie=${AOC_COOKIE}
day_name=$(printf "Day%02d" "$day")
full_path=$year/$day_name

# Create folder
echo -e "> Copying template to $full_path"
mkdir -p "$full_path"
cp "$ROOT"/template/solution.hs "$full_path/"
cp "$ROOT"/template/Tools.hs "$full_path/"

# Download inputs
echo -e "> Downloading input file"
curl --silent 'https://adventofcode.com/'"$year"'/day/'"$day"'/input' -H 'cookie: '"$aoc_cookie" > "$full_path/input.txt"

echo "Link to problem: https://adventofcode.com/$year/day/$day"
echo -e "--------------------"
echo -e "> DONE!"
