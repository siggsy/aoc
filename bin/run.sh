#!/usr/bin/env bash

file=${1:-input.txt}
runghc solution.hs "${@:2}" < "$file"
