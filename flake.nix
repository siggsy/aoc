{
  description = "Advent Of Code flake for haskell";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs =
    { nixpkgs, ... }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
      ghc = pkgs.haskellPackages.ghcWithPackages (
        p: with p; [
          attoparsec
          haskell-language-server
          pretty-simple
          monad-memo
          hoogle
        ]
      );
    in
    {
      devShells.x86_64-linux.default = pkgs.mkShell {
        packages = [ ghc pkgs.ormolu ];
        shellHook = ''
          export PATH=$PATH:$(pwd)/bin
        '';
      };
    };
}
