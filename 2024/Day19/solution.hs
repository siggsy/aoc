{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.Memo (Memo, lift, memo, runMemo, startEvalMemo)
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.List (inits, tails)
import qualified Data.Map.Strict as M
import qualified Data.Set as S
import Debug.Trace (trace)
import qualified Tools as T

------------ [ Types ] ---------------
type Input = ([String], [String])

------------ [ Parse ] ---------------
parseInput :: String -> IO Input
parseInput = T.runParser $ (,) <$> parseTowels <*> (P.endOfLine >> P.endOfLine >> parsePatterns)
  where
    parseTowels = P.many1 P.letter `P.sepBy1` P.string ", "
    parsePatterns = P.many1 P.letter `P.sepBy1` P.endOfLine

------------ [ Main ] ----------------
main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "+---------------------------+"
  putStrLn "| Part A:                   |"
  putStrLn "+---------------------------+"
  partA input
  putStrLn ""

  putStrLn "+---------------------------+"
  putStrLn "| Part B:                   |"
  putStrLn "+---------------------------+"
  partB input

------------ [ Part A ] ---------------
partA :: Input -> IO ()
partA (towels, patterns) = do
  print $ length . filter id . map (not . null) $ tryPattern towels <$> patterns

tryPattern :: [String] -> String -> [[String]]
tryPattern towels patt = go patt
  where
    towelSet = S.fromList towels

    go :: String -> [[String]]
    go [] = [[]]
    go patt =
      concat
        [ (towel :) <$> go patt'
          | (towel, patt') <- zip (tails patt) (inits patt),
            towel `S.member` towelSet
        ]

------------ [ Part B ] ---------------
partB :: Input -> IO ()
partB (towels, patterns) = do
  print $ sum $ countOptions towels <$> patterns

countOptions :: [String] -> String -> Int
countOptions towels patt = startEvalMemo $ go patt
  where
    towelSet = S.fromList towels

    go :: String -> Memo String Int Int
    go [] = return 1
    go patt =
      sum
        <$> sequence
          [ memo go patt'
            | (towel, patt') <- zip (tails patt) (inits patt),
              towel `S.member` towelSet
          ]
