{-# LANGUAGE OverloadedStrings #-}

import Control.Applicative ((<|>))
import Control.Monad.State.Strict
  ( State,
    evalState,
    execState,
    get,
    gets,
    modify,
    runState,
  )
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.Char (isAlpha, isDigit)
import Data.List (isPrefixOf, intercalate)
import Data.List.Extra (sortOn)
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.Ord
import qualified Data.Ord
import Data.Set (Set)
import qualified Data.Set as S
import Debug.Trace (trace)
import qualified Tools as T
import Data.List (sort)

------------------------------------------------------------
-- Types
------------------------------------------------------------

data Input = Input
  { vars :: !(Map String Bool),
    assignments :: ![(String, Expr)]
  }
  deriving (Show)

data Expr
  = AND !String !String
  | OR !String !String
  | XOR !String !String
  deriving (Show)

varsOf :: Expr -> [String]
varsOf (AND x y) = [x, y]
varsOf (OR x y) = [x, y]
varsOf (XOR x y) = [x, y]

------------------------------------------------------------
-- Parse
------------------------------------------------------------

parseInput :: String -> IO Input
parseInput =
  T.runParser $
    Input
      <$> (M.fromList <$> (parseVar `P.sepBy1` P.endOfLine))
      <*> (P.endOfLine >> P.endOfLine >> (parseAssignment `P.sepBy1` P.endOfLine))
  where
    word = P.many1' (P.digit <|> P.letter)
    parseVar = do
      var <- word
      P.string ": "
      bit <- P.digit
      return (var, bit == '1')

    parseAssignment = do
      x <- word
      P.char ' '
      op <- word
      P.char ' '
      y <- word
      P.string " -> "
      z <- word
      let op' = case op of
            "AND" -> AND
            "XOR" -> XOR
            "OR" -> OR
      return (z, op' x y)

------------------------------------------------------------
-- Main
------------------------------------------------------------

main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "+---------------------------+"
  putStrLn "| Part A:                   |"
  putStrLn "+---------------------------+"
  partA input
  putStrLn ""

  putStrLn "+---------------------------+"
  putStrLn "| Part B:                   |"
  putStrLn "+---------------------------+"
  partB input

------------------------------------------------------------
-- Part A
------------------------------------------------------------

partA :: Input -> IO ()
partA input = do
  T.pretty $
    toNum $
      map snd . sortOn fst . filter (\(v, _) -> "z" `isPrefixOf` v) . M.toList $
        eval (assignments input) (vars input)

-- >>> toNum [False, False, True]
-- 4
toNum :: [Bool] -> Int
toNum = go 1
  where
    go n [] = 0
    go n (b : bs) = n * fromEnum b + go (n * 2) bs

eval :: [(String, Expr)] -> Map String Bool -> Map String Bool
eval exprs variables = go variables (filter (`M.member` exprsMap) $ topologicalSort exprs)
  where
    exprsMap = M.fromList exprs

    go vars [] = vars
    go vars (v : es) =
      let res = case exprsMap M.! v of
            AND x y -> (vars M.! x) && (vars M.! y)
            OR x y -> (vars M.! x) || (vars M.! y)
            XOR x y -> (vars M.! x) /= (vars M.! y)
       in go (M.insert v res vars) es

-- >>> topologicalSort [("z", OR "x" "y"), ("w", AND "z" "r"), ("r", XOR "x" "z")]
-- ["y","x","z","r","w"]
topologicalSort :: [(String, Expr)] -> [String]
topologicalSort assignments = evalState (go []) nodes
  where
    nodes = S.fromList . concatMap (\(v, e) -> v : varsOf e) $ assignments
    edges = S.fromList . concatMap (\(v, e) -> (,v) <$> varsOf e) $ assignments

    neighbs v = S.fromList $ map snd . filter (\(v', _) -> v' == v) $ S.toList edges

    go :: [String] -> State (Set String) [String]
    go stack = do
      v <- gets S.findMin
      stack' <- dfs v
      unvisited <- get
      if S.null unvisited
        then return $ stack' ++ stack
        else go (stack' ++ stack)

    dfs :: String -> State (Set String) [String]
    dfs v =
      (v :) . concat . reverse
        <$> ( modify (S.delete v)
                >> get
                >>= traverse dfs
                  . S.toList
                  . S.intersection (neighbs v)
            )

------------------------------------------------------------
-- Part B
------------------------------------------------------------

partB :: Input -> IO ()
partB input = do
  print $ intercalate "," . sort . map fst . validate $ assignments input

validate :: [(String, Expr)] -> [(String, Expr)]
validate exprs = filter (not . isValid) $ exprs
  where
    isValid ('z' : _, XOR x y) = True
    isValid ("z45", _) = True
    isValid ('z': _, _) = False
    isValid (res, XOR x@(_x:_) y@(_y:_))
      | not (_x `elem` [ 'x', 'y' ] || _y `elem` [ 'x', 'y' ]) = False
      | otherwise = not . any isOR . filter (res `partOf`) $ exprs

    isValid (res, AND x y)
      | "x00" `elem` [x, y] = True
      | otherwise = all isOR . filter (res `partOf`) $ exprs

    isValid _ = True

    v `partOf` (_, e) = v `elem` varsOf e

    isOR (_, OR _ _) = True
    isOR _ = False
