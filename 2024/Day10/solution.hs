{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.State
import Data.Array.IArray (Array, (!), (!?))
import qualified Data.Array.IArray as A
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.Char (digitToInt)
import Data.List (foldl')
import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as M
import Data.Maybe (Maybe (Just), catMaybes, fromMaybe)
import Data.Set (Set)
import qualified Data.Set as S
import Data.Text (pack)
import Text.Pretty.Simple (OutputOptions (..), defaultOutputOptionsNoColor, pPrintOpt)
import Text.Pretty.Simple.Internal.Printer (CheckColorTty (..))
import qualified Tools as T

pretty :: (Show a) => a -> IO ()
pretty =
  pPrintOpt
    NoCheckColorTty
    defaultOutputOptionsNoColor
      { outputOptionsCompact = True
      }

main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "Parsed:"
  putStrLn (T.printArray input)
  putStrLn "==============================="

  putStrLn "Part A: "
  pretty (partA input)
  putStrLn ""

  putStrLn "Part B: "
  pretty (partB input)

------------ [ Input ] ---------------
type Input = Array (Int, Int) Int

------------ [ Parse ] ---------------
runParser :: Parser Input -> String -> IO Input
runParser p s = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

parseInput :: String -> IO Input
parseInput s = pure topographicMap
  where
    x = length . head . lines $ s
    y = length . lines $ s

    topographicMap =
      A.listArray
        ((0, 0), (y - 1, x - 1))
        (map digitToInt . concat . lines $ s)

------------ [ Part A ] ---------------
partA topMap = sum . map S.size $ runMemo (mapM (reach9s topMap) (zeros topMap))

type Memo a b = State (Map a b) b

memo :: (Ord a) => (a -> Memo a b) -> a -> Memo a b
memo f arg = do
  memoized <- gets (M.lookup arg)
  case memoized of
    Just m -> return m
    Nothing -> do
      res <- f arg
      modify (M.insert arg res)
      return res

runMemo m = evalState m M.empty

zeros :: Array (Int, Int) Int -> [(Int, Int)]
zeros topMap = map fst . filter ((== 0) . snd) $ A.assocs topMap

reach9s :: Array (Int, Int) Int -> (Int, Int) -> Memo (Int, Int) (Set (Int, Int))
reach9s topMap (y, x) =
  case topMap ! (y, x) of
    9 -> return (S.singleton (y, x))
    h -> S.unions <$> sequence explored
      where
        explored =
          [ memo (reach9s topMap) pos'
            | pos' <- [(y - 1, x), (y, x + 1), (y + 1, x), (y, x - 1)],
              Just (h + 1) == (topMap !? pos')
          ]

------------ [ Part B ] ---------------
partB topMap = sum $ runMemo (mapM (rank9s topMap) (zeros topMap))

rank9s :: Array (Int, Int) Int -> (Int, Int) -> Memo (Int, Int) Int
rank9s topMap (y, x) =
  case topMap ! (y, x) of
    9 -> return 1
    h -> sum <$> sequence explored
      where
        explored =
          [ memo (rank9s topMap) pos'
            | pos' <- [(y - 1, x), (y, x + 1), (y + 1, x), (y, x - 1)],
              Just (h + 1) == (topMap !? pos')
          ]
