{-# LANGUAGE ViewPatterns #-}

module Tools
  ( pretty,
    runParser,
    parseMap,
    Coordinate (..),
    fromPair,
    toPair,
    offsetBy,
    crossAround,
    xAround,
    starAround,
    bounded,
    reachable,
    dijkstra,
    showMap,
    detectCycle
  )
where

import Control.Monad (guard, when)
import qualified Data.Array as A
import Data.Array.IArray (Array, listArray, (!))
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.Heap (Entry (..), Heap)
import qualified Data.Heap as H
import Data.IntMap (IntMap)
import qualified Data.IntMap as IM
import Data.IntSet (IntSet)
import qualified Data.IntSet as IS
import Data.List.Extra (intersperse, merge)
import qualified Data.Map as M
import Data.Maybe (fromMaybe, mapMaybe)
import Data.Semiring (IntMapOf (IntMapOf))
import Data.Set (Set)
import Data.Text (pack)
import Data.Tuple (swap)
import GHC.Ix (Ix, inRange, index, range, unsafeIndex)
import Text.Pretty.Simple (OutputOptions (..), defaultOutputOptionsNoColor, pPrintOpt)
import Text.Pretty.Simple.Internal.Printer (CheckColorTty (..))

-------------------------- [ Input helpers ] ------------------------------------

pretty :: (Show a) => a -> IO ()
pretty =
  pPrintOpt
    NoCheckColorTty
    defaultOutputOptionsNoColor
      { outputOptionsCompact = True
      }

runParser :: Parser a -> String -> IO a
runParser p s = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

-------------------------- [ Dealing with maps ] --------------------------------

infix 0 :.

data Coordinate = !Int :. !Int
  deriving (Eq)

fromPair :: (Int, Int) -> Coordinate
fromPair (y, x) = x :. y

toPair :: Coordinate -> (Int, Int)
toPair (x :. y) = (y, x)

instance Show Coordinate where
  showsPrec _ (x :. y) = showChar '<' . shows x . showString ", " . shows y . showChar '>'

instance Ord Coordinate where
  compare (x :. y) (a :. b) = compare (y, x) (b, a)

instance Ix Coordinate where
  range (x :. y, a :. b) = map (uncurry (:.) . swap) (range ((y, x), (b, a)))
  inRange (x :. y, a :. b) (i :. j) = inRange ((y, x), (b, a)) (j, i)
  index (x :. y, a :. b) (i :. j) = index ((y, x), (b, a)) (j, i)

offsetBy :: Coordinate -> Coordinate -> Coordinate
offsetBy (x :. y) (dx :. dy) = x + dx :. y + dy

parseMap :: (Char -> a) -> String -> Array Coordinate a
parseMap f s = listArray (0 :. 0, x - 1 :. y - 1) m
  where
    m = (map f . concat) (lines s)
    x = (length . head) (lines s)
    y = length (lines s)

crossAround :: Coordinate -> [Coordinate]
crossAround (x :. y) =
  [ x :. y - 1,
    x - 1 :. y,
    x + 1 :. y,
    x :. y + 1
  ]

xAround :: Coordinate -> [Coordinate]
xAround (x :. y) =
  [ x - 1 :. y - 1,
    x + 1 :. y - 1,
    x - 1 :. y + 1,
    x + 1 :. y + 1
  ]

starAround :: Coordinate -> [Coordinate]
starAround coor = merge (crossAround coor) (xAround coor)

bounded :: (Coordinate, Coordinate) -> (Coordinate -> [Coordinate]) -> Coordinate -> [Coordinate]
bounded bounds f = filter (A.inRange bounds) . f

reachable :: (Coordinate, Coordinate) -> (Coordinate -> [Coordinate]) -> Coordinate -> [Coordinate]
reachable bounds next curr = map toCoordinate (IS.toList indices)
  where
    toIndex = unsafeIndex bounds
    indices = go IS.empty curr

    (sx :. _, ex :. _) = bounds

    toCoordinate :: Int -> Coordinate
    toCoordinate i =
      let dx = (ex - sx)
       in i `mod` dx :. i `div` dx

    go :: IntSet -> Coordinate -> IntSet
    go visited pos
      | toIndex pos `IS.member` visited = visited
      | otherwise =
          foldr
            (flip go)
            (IS.insert (toIndex pos) visited)
            (filter (inRange bounds) (next pos))

-- | Dijkstra path finding algorithm
dijkstra ::
  (Ord v) =>
  -- | Index function
  (v -> Int) ->
  -- | Neighbours function
  (v -> [v]) ->
  -- | Cost function
  (v -> v -> Int) ->
  -- | Set of end coordinates
  (v -> Bool) ->
  -- | Where to start
  v ->
  -- | Found path from end -> start
  Maybe [v]
dijkstra index neighbs cost ends start =
  search
    (IM.singleton (index start) 0)
    IM.empty
    (H.singleton (Entry 0 start))
  where
    constructPath u prevMap =
      case prevMap IM.!? index u of
        Just v -> u : constructPath v prevMap
        Nothing -> [u]

    maybeEnqueue distances u v =
      let alt = cost u v
       in case distances IM.!? index v of
            Just d
              | alt < d -> return $ Entry alt v
              | otherwise -> Nothing
            Nothing -> return $ Entry alt v

    search distances prevMap (H.viewMin -> Just (Entry c u, queue))
      | ends u = return (constructPath u prevMap)
      | otherwise =
          let toQueue = mapMaybe (maybeEnqueue distances u) (neighbs u)

              queue' = H.fromList toQueue <> queue
              distances' = (IM.fromList . map (\(Entry c v) -> (index v, c)) $ toQueue) <> distances
              prevMap' = (IM.fromList . map (\(Entry c v) -> (index v, u)) $ toQueue) <> prevMap
           in search distances' prevMap' queue'

-- | Shows map based on provided coordinate bounds and lookup function
-- ==== __Examples__
--
-- >>> showMap (0 :. 0, 3 :. 3) (const '.')
-- ". . . .\n. . . .\n. . . .\n. . . .\n"
-- >>> showMap (0 :. 0, 3 :. 3) (\(x :. y) -> if x == y then '#' else '.')
-- "# . . .\n. # . .\n. . # .\n. . . #\n"
showMap :: (Coordinate, Coordinate) -> (Coordinate -> Char) -> String
showMap (sx :. sy, ex :. ey) lookup =
  let arrayLines =
        [ intersperse
            ' '
            [ lookup (x :. y)
              | x <- [sx .. ex]
            ]
          | y <- [sy .. ey]
        ]
   in unlines arrayLines

-------------------------- [ There always has to be a cycle ] -------------------

detectCycle :: (Eq a) => [a] -> ([a], [a])
detectCycle as = cycle as as
  where
    cycle (x : xs) (_ : y : ys)
      | x == y = start as xs
      | otherwise = cycle xs ys
    cycle _ _ = (as, [])

    start (x : xs) (y : ys)
      | x == y = ([], x : cyc x xs)
      | otherwise = let (xs', ys') = start xs ys in (x : xs', ys')

    cyc x (y : ys)
      | x == y = []
      | otherwise = y : cyc x ys
