import Control.Monad (foldM, foldM_, when, forM_)
import Control.Monad.Extra (liftM2)
import Control.Monad.ST (ST, runST)
import Control.Monad.State (lift)
import Control.Monad.Trans.Maybe (MaybeT (runMaybeT))
import Data.Array.IArray (Array, IArray (bounds), assocs, elems, listArray, (!))
import Data.Array.ST (STArray, freeze, readArray, runSTArray, thaw, writeArray)
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.Maybe (fromMaybe, isJust, mapMaybe)
import Data.Primitive.Array (createArray)
import Tools (Coordinate (..))
import qualified Tools as T
import Data.List (inits, nub)
import Control.Concurrent (threadDelay)
import Text.Printf (printf)
import Data.List.Extra (nubOrd)

------------ [ Types ] ---------------
type Input = (BoxMap, [Movement])

type BoxMap = Array Coordinate Tile

type Tile = Char

type Movement = Char

type MaybeST s a = MaybeT (ST s) a

------------ [ Parse ] ---------------
parseInput :: String -> IO Input
parseInput i =
  pure $
    let (m, mov) = span (/= []) . lines $ i
     in (T.parseMap id (unlines m), filter (/= '\n') . unlines $ mov)

------------ [ Main ] ----------------
main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "-----------------------------"
  putStrLn "| Part A:                   |"
  putStrLn "-----------------------------"
  partA input
  putStrLn ""

  putStrLn "-----------------------------"
  putStrLn "| Part B:                   |"
  putStrLn "-----------------------------"
  partB input

------------ [ Part A ] ---------------
partA :: Input -> IO ()
partA (bm, moves) = do
  let bm' = runMoves bm moves
  printBoxes bm'
  print $ sum . map (gpsCoordinate . fst) . filter (\(_, t) -> t == 'O') $ assocs bm'

tryMove :: STArray s Coordinate Char -> Coordinate -> Coordinate -> Char -> ST s (Maybe Coordinate)
tryMove arr pos dir object = do
  let pos' = pos `T.offsetBy` dir
  t <- readArray arr pos'
  case t of
    '#' -> return Nothing
    'O' -> do
      moved <- tryMove arr pos' dir 'O'
      when (isJust moved) (move pos')
      return $ moved >> return pos'
    '.' -> do
      move pos'
      return $ Just pos'
  where
    move pos' = do
      writeArray arr pos '.'
      writeArray arr pos' object

runMoves :: BoxMap -> [Movement] -> BoxMap
runMoves bm movs =
  let (robotPos : _) = mapMaybe (\(i, a) -> if a == '@' then Just i else Nothing) $ assocs bm
   in runST $ do
        arr <- thaw bm
        go arr robotPos movs
        freeze arr
  where
    go arr pos [] = return ()
    go arr pos (m : ms) = do
      pos' <- fromMaybe pos <$> tryMove arr pos (getDir m) '@'
      go arr pos' ms

getDir :: Char -> Coordinate
getDir d = case d of
  '^' -> 0 :. -1
  '>' -> 1 :. 0
  'v' -> 0 :. 1
  '<' -> -1 :. 0

robotPos :: BoxMap -> Coordinate
robotPos bm =
  let (robotPos : _) = mapMaybe (\(i, a) -> if a == '@' then Just i else Nothing) $ assocs bm
   in robotPos

gpsCoordinate :: Coordinate -> Int
gpsCoordinate (x :. y) = y * 100 + x

printBoxes :: BoxMap -> IO ()
printBoxes bm = putStrLn $ T.showMap (bounds bm) (bm !)

------------ [ Part B ] ---------------
partB :: Input -> IO ()
partB (bm, moves) = do
  let dbm = doubleWidth bm
  printBoxes dbm
  let dbm' = runMoves' dbm moves
  printBoxes dbm'
  print $ sum . map (gpsCoordinate . fst) . filter (\(_, t) -> t == '[') $ assocs dbm'

movable :: STArray s Coordinate Char -> Coordinate -> Coordinate -> MaybeST s [Coordinate]
movable arr pos dir = do
  t <- lift $ readArray arr pos

  let pos' = pos `T.offsetBy` dir
  case t of
    '#' -> fail "Hit a wall"
    '@' -> (pos :) <$> movable arr pos' dir
    '[' -> canMoveBox pos (pos `T.offsetBy` (1 :. 0))
    ']' -> canMoveBox (pos `T.offsetBy` (-1 :. 0)) pos
    '.' -> return []
  where
    canMoveBox l r = case dir of
      (0 :. y) -> do
        left <- movable arr (l `T.offsetBy` dir) dir
        right <- movable arr (r `T.offsetBy` dir) dir
        return $ l : r : right ++ left
      (-1 :. 0) -> do
        left <- movable arr (l `T.offsetBy` dir) dir
        return $ r : l : left
      (1 :. 0) -> do
        right <- movable arr (r `T.offsetBy` dir) dir
        return $ l : r : right

tryMove' :: STArray s Coordinate Tile -> Coordinate -> Coordinate -> MaybeST s Coordinate
tryMove' arr pos dir = do
  let pos' = pos `T.offsetBy` dir
  toMove <- movable arr pos dir
  lift $ mapM_ (move dir) (nubOrd . reverse $ toMove)
  pure pos'
  where
    move dir pos = do
      let pos' = pos `T.offsetBy` dir
      t <- readArray arr pos
      writeArray arr pos '.'
      writeArray arr pos' t

runMoves' :: BoxMap -> [Movement] -> BoxMap
runMoves' bm moves =
  let dirs = map getDir moves
      pos = robotPos bm
   in runSTArray $ do
        arr <- thaw bm
        foldM_
          (\pos dir -> fromMaybe pos <$> runMaybeT (tryMove' arr pos dir))
          pos
          dirs
        return arr

doubleWidth :: BoxMap -> BoxMap
doubleWidth bm =
  let (_, x :. y) = bounds bm
   in listArray (0 :. 0, (x + 1) * 2 - 1 :. y) (concatMap stretch $ elems bm)
  where
    stretch '#' = "##"
    stretch 'O' = "[]"
    stretch '.' = ".."
    stretch '@' = "@."
