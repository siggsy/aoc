{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (Parser)
import Data.Attoparsec.Text qualified as P
import Data.Text (pack)

import Data.Map.Lazy (Map)
import Data.Map.Lazy qualified as M
import Data.Set (Set)
import Data.Set qualified as S

import Data.Array (Array, (!))
import Data.Array qualified as A

import Tools qualified as T

main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "Parsed:"
  putStrLn (T.printArray input)
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)

------------ [ Input ] ---------------
type Input = Array (Int, Int) Char

------------ [ Parse ] ---------------
runParser :: Parser Input -> String -> IO Input
runParser p s = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

parseInput :: String -> IO Input
parseInput s =
  pure $
    A.listArray
      ((0, 0), (y - 1, x - 1))
      (filter (/= '\n') s)
 where
  x = length . head . lines $ s
  y = length . lines $ s

------------ [ Part A ] ---------------
-- partA ws = findFrom ws (4,4)
partA ws =
  sum
    [ length words
    | i <- A.indices ws
    , let words = filter (=="XMAS") $ findFrom ws i ]

findFrom :: Input -> (Int, Int) -> [String]
findFrom ws (y,x) = map (map (ws !)) posibilities
 where
   diffs = [dDiagPlus, dHorizontal, dDiagMinus, dVertical]
   posibilities = map (filter (A.inRange (A.bounds ws)) . map applyDiff) $ diffs ++ map rev diffs
   dDiagPlus = zip [0..3] [0..3]
   dDiagMinus = zip [0, -1 .. -3 ] [0..3]
   dHorizontal = map (0,) [0..3]
   dVertical = map (,0) [0..3]

   applyDiff (dx,dy) = (x+dx, y+dy)

   rev [] = []
   rev ((y,x):xs) = (-y, -x) : rev xs

------------ [ Part B ] ---------------
partB ws =
  length
    [ pos
    | pos@(y,x) <- A.indices ws
    , let ((sy, sx), (ey, ex)) = A.bounds ws
    , y > sy, x > sx
    , y < ey, x < ex
    , isX ws pos
    ]

isX :: Input -> (Int, Int) -> Bool
isX ws (y,x) = start == 'A' && case corners of
  ['M', 'M', _, _]
    | topLeft == "MAS" && topRight == "MAS" -> True
  ['M', _, 'M', _]
    | topLeft == "MAS" && bottomLeft == "MAS" -> True
  [_, 'M', _, 'M']
    | topRight == "MAS" && bottomRight == "MAS" -> True
  [_, _, 'M', 'M']
    | bottomLeft == "MAS" && bottomRight == "MAS" -> True
  _none -> False
 where
  start = ws ! (y,x)
  corners = map (ws !) [ (y+1, x-1), (y+1, x+1), (y-1, x-1), (y-1, x+1) ]
  topLeft  = wordIn [1, 0, -1] [-1, 0, 1]
  topRight = wordIn [1, 0, -1] [1, 0, -1]
  bottomLeft  = wordIn [-1, 0, 1] [-1, 0, 1]
  bottomRight = wordIn [-1, 0, 1] [1, 0, -1]

  wordIn = zipWith (\dy dx -> ws ! (y+dy, x+dx))
