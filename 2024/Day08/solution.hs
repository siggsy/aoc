{-# LANGUAGE OverloadedStrings #-}

import Data.Array (Array)
import qualified Data.Array as A
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.List (nub)
import Data.List.Extra (nubOrd)
import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as M
import Data.Set (Set)
import qualified Data.Set as S
import Data.Text (pack)
import Text.Pretty.Simple (OutputOptions (..), defaultOutputOptionsNoColor, pPrintOpt)
import Text.Pretty.Simple.Internal.Printer (CheckColorTty (..))
import qualified Tools as T

pretty :: (Show a) => a -> IO ()
pretty =
  pPrintOpt
    NoCheckColorTty
    defaultOutputOptionsNoColor
      { outputOptionsCompact = True
      }

main :: IO ()
main = do
  input@(m, _) <- getContents >>= parseInput
  putStrLn "Parsed:"
  putStrLn $ T.printArray m
  putStrLn "==============================="

  putStrLn "Part A: "
  pretty (partA input)
  putStrLn ""

  putStrLn "Part B: "
  pretty (partB input)

------------ [ Input ] ---------------
type Input = (AntennaMap, Set Antenna)

type Pos = (Int, Int)

type AntennaMap = Array Pos Char

type Antenna = Char

------------ [ Parse ] ---------------
runParser :: Parser Input -> String -> IO Input
runParser p s = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

parseInput :: String -> IO Input
parseInput s = pure $ (layout, antennas)
  where
    x = length . head . lines $ s
    y = length . lines $ s

    rawLines = concat . lines $ s
    layout = A.listArray ((0, 0), (y - 1, x - 1)) rawLines
    antennas = S.fromList (filter (/= '.') rawLines)

------------ [ Part A ] ---------------
partA (m, as) =
  countUniq
    . concat
    $ [filter (A.inRange (A.bounds m)) [ax, ay] | (_, _ : ax : _, _ : ay : _) <- antinodes m as]

antennaLocations :: AntennaMap -> Antenna -> [Pos]
antennaLocations m a = [pos | (pos, a') <- A.assocs m, a == a']

resAntinodesWith :: Pos -> Pos -> ([Pos], [Pos])
(ay, ax) `resAntinodesWith` (by, bx) = (above, below)
  where
    above = [(ay + k * dy, ax + k * dx) | k <- [0 ..]]
    below = [(by - k * dy, bx - k * dx) | k <- [0 ..]]
    dy = ay - by
    dx = ax - bx

antinodes :: AntennaMap -> Set Antenna -> [(Antenna, [Pos], [Pos])]
antinodes m as =
  [ (a, ax, ay)
    | a <- S.toList as,
      let antennaLocs = antennaLocations m a,
      x <- antennaLocs,
      y <- antennaLocs,
      x /= y,
      let (ax, ay) = x `resAntinodesWith` y
  ]

countUniq :: (Ord a) => [a] -> Int
countUniq = length . nubOrd

------------ [ Part B ] ---------------
partB (m, as) =
  countUniq
    . concat
    $ [takeInBounds ax ++ takeInBounds ay | (_, ax, ay) <- antinodes m as]
  where
    takeInBounds = takeWhile (A.inRange (A.bounds m))
