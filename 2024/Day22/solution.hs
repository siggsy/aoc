{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.Bits (shiftL, shiftR, xor)
import Data.List (iterate', zipWith4, foldl')
import qualified Data.HashMap.Strict as M
import qualified Tools as T
import Data.HashMap.Strict (HashMap)

------------ [ Types ] ---------------
type Input = [Int]

type Sequence = (Int, Int, Int, Int)

------------ [ Parse ] ---------------
parseInput :: String -> IO Input
parseInput = pure . map read . lines

------------ [ Main ] ----------------
main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "+---------------------------+"
  putStrLn "| Part A:                   |"
  putStrLn "+---------------------------+"
  partA input
  putStrLn ""

  putStrLn "+---------------------------+"
  putStrLn "| Part B:                   |"
  putStrLn "+---------------------------+"
  partB input

------------ [ Part A ] ---------------
partA :: Input -> IO ()
partA = print . sum . map (\i -> iterate' step i !! 2000)

p :: Int
p = 16777216

-- >>> take 11 (iterate step 123)
-- [123,15887950,16495136,527345,704524,1553684,12683156,11100544,12249484,7753432,5908254]
step :: Int -> Int
step = s3 . s2 . s1
  where
    s1 n = (n `shiftL` 6 `xor` n) `mod` p
    s2 n = (n `shiftR` 5 `xor` n) `mod` p
    s3 n = (n `shiftL` 11 `xor` n) `mod` p

bananas :: Int -> Int
bananas n = n `mod` 10

prices :: Int -> [Int]
prices = iterate step

-- >>> take 11 (changes 123)
-- [(0,-3),(6,6),(5,-1),(4,-1),(4,0),(6,2),(4,-2),(4,0),(2,-2),(4,2),(0,-4)]
changes :: Int -> [(Int, Int)]
changes n =
  let ps = map bananas (prices n)
   in zip (drop 1 ps) (zipWith (-) (drop 1 ps) ps)

-- >>> seqMap (take 11 (changes 123))
-- fromList [((-3,6,-1,-1),4),((0,2,-2,0),4),((6,-1,-1,0),4),((0,-2,2,-4),0),((-1,0,2,-2),4),((2,-2,0,-2),2),((-1,-1,0,2),6),((-2,0,-2,2),4)]
seqMap :: [(Int, Int)] -> HashMap Sequence Int
seqMap cs =
  M.fromListWith (\_ x -> x) $
    zipWith4
      (\(_, c1) (_, c2) (_, c3) (d, c4) -> ((c1, c2, c3, c4), d))
      cs
      (drop 1 cs)
      (drop 2 cs)
      (drop 3 cs)

------------ [ Part B ] ---------------
partB :: Input -> IO ()
partB input =
  print
    $ maximum
      . M.elems
      . foldl' (M.unionWith (+)) M.empty
      . map (seqMap . take 2000 . changes)
    $ input
