{-# LANGUAGE OverloadedStrings #-}

import Control.Applicative ((<|>))
import Data.Attoparsec.Text (Parser)
import Data.Attoparsec.Text qualified as P
import Data.Functor (($>))
import Data.Text (pack)

import Data.Map.Lazy (Map)
import Data.Map.Lazy qualified as M
import Data.Set (Set)
import Data.Set qualified as S

import Tools qualified as T

main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)

------------ [ Input ] ---------------
type Input = [Instruction]
data Instruction = Mul (Int, Int) | Do Bool deriving (Show)

runInstructions :: Bool -> [Instruction] -> Int
runInstructions ignoreDoDonts is = go True is
 where
  go _ [] = 0
  go True (Mul (x, y) : is) = x * y + go True is
  go False (Mul (x, y) : is) = go False is
  go enabled (Do enable : is)
    | ignoreDoDonts = go enabled is
    | otherwise = go enable is

------------ [ Parse ] ---------------
runParser :: Parser Input -> String -> IO Input
runParser p s = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

parseInput :: String -> IO Input
parseInput = runParser $ P.many1 parseInstruction

parseInstruction :: Parser Instruction
parseInstruction = do
  P.takeWhile (\c -> c /= 'm' && c /= 'd')
  P.try mulI <|> P.try doI <|> P.try dontI <|> (P.anyChar >> parseInstruction)
 where
  mulI = do
    P.string "mul("
    x <- P.many1 P.digit
    P.char ','
    y <- P.many1 P.digit
    P.char ')'
    return $ Mul (read x, read y)

  doI = P.string "do()" $> Do True
  dontI = P.string "don't()" $> Do False

------------ [ Part A ] ---------------
partA = runInstructions True

------------ [ Part B ] ---------------
partB = runInstructions False
