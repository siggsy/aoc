{-# LANGUAGE OverloadedStrings #-}

import Data.Array.IArray (Array, IArray (bounds), (!))
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.Either (partitionEithers)
import Data.List.Extra (splitOn)
import System.Environment (getArgs)
import Tools (Coordinate (..))
import qualified Tools as T

--  ,----------------------------------------------------------------------------
--  | Types: types that we will use in the solution (mostly input)
--  '----------------------------------------------------------------------------

type Input = [Array Coordinate Char]

--  ,----------------------------------------------------------------------------
--  | Parse: attoparsec / manual parsing
--  '----------------------------------------------------------------------------

parseInput :: String -> IO Input
parseInput = pure . map (T.parseMap id) . splitOn "\n\n"

--  ,----------------------------------------------------------------------------
--  | Main: run the parser and pass it to partA and partB
--  '----------------------------------------------------------------------------

main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn ",--------------------------"
  putStrLn "| Part A:                  "
  putStrLn "'--------------------------"
  partA input
  putStrLn ""

--  ,----------------------------------------------------------------------------
--  | Part A
--  '----------------------------------------------------------------------------

partA :: Input -> IO ()
partA input = do
  let heights = map toHeights input
  print (countFitting heights)

countFitting :: [Either [Int] [Int]] -> Int
countFitting heights =
  length
    . filter and
    . (\(locks, keys) -> zipWith (\l k -> l + k <= 5) <$> locks <*> keys)
    $ partitionEithers heights

toHeights :: Array Coordinate Char -> Either [Int] [Int]
toHeights scheme
  | scheme ! (0 :. 0) == '#' = Left (heights [sy .. ey])
  | otherwise = Right (heights [ey, ey - 1 .. sy])
  where
    (sx :. sy, ex :. ey) = bounds scheme
    heights ys =
      pred
        . length
        . takeWhile (== '#')
        . map (scheme !)
        . (\x -> map (x :.) ys)
        <$> [sx .. ex]
