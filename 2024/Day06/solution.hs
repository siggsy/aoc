{-# LANGUAGE OverloadedStrings #-}

import Data.Array (Array, (!))
import qualified Data.Array as A
import Data.Array.IArray ((!?))
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as M
import Data.Set (Set)
import qualified Data.Set as S
import Data.Text (pack)
import Text.Pretty.Simple (OutputOptions (..), defaultOutputOptionsNoColor, pPrintOpt)
import Text.Pretty.Simple.Internal.Printer (CheckColorTty (..))
import qualified Tools as T

pretty :: (Show a) => a -> IO ()
pretty =
  pPrintOpt
    NoCheckColorTty
    defaultOutputOptionsNoColor
      { outputOptionsCompact = True
      }

main :: IO ()
main = do
  input@(_, _, tiles) <- getContents >>= parseInput
  putStrLn "Parsed:"
  putStrLn (T.printArray (fmap showTile tiles))
  putStrLn "==============================="

  putStrLn "Part A: "
  pretty (partA input)
  putStrLn ""

  putStrLn "Part B: "
  pretty (partB input)

------------ [ Input ] ---------------
type Input = (Pos, Direction, Array Pos Tile)

type Tile = Bool

showTile True = '#'
showTile False = '.'

type Pos = (Int, Int)

data Direction = N | W | S | E deriving (Show, Ord, Eq)

------------ [ Parse ] ---------------
runParser :: Parser Input -> String -> IO Input
runParser p s = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

parseInput :: String -> IO Input
parseInput s = pure (guardPosition, N, tiles)
  where
    (ey, ex) = (length $ lines s, length . head $ lines s)
    guardPosition =
      head
        [ (y, x)
          | (y, l) <- zip [0 ..] (lines s),
            (x, tile) <- zip [0 ..] l,
            tile == '^'
        ]

    tiles = A.listArray ((0, 0), (ey - 1, ex - 1)) (map isObstruction . filter (/= '\n') $ s)
    isObstruction '#' = True
    isObstruction _ = False

------------ [ Part A ] ---------------
partA (start, direction, tiles) = S.size . S.fromList $ path tiles start direction

path :: Array Pos Tile -> Pos -> Direction -> [(Pos, Direction)]
path tiles start direction = case tiles !? (start `towards` direction) of
  Nothing -> [(start, direction)]
  Just True -> path tiles start (turnRight direction)
  Just False -> (start, direction) : path tiles (start `towards` direction) direction

towards :: Pos -> Direction -> Pos
towards (y, x) direction = case direction of
  N -> (y - 1, x)
  W -> (y, x + 1)
  S -> (y + 1, x)
  E -> (y, x - 1)

turnRight N = W
turnRight W = S
turnRight S = E
turnRight E = N

------------ [ Part B ] ---------------
partB (start, direction, tiles) =
  length
    [ (pos, cycle)
      | pos <- A.indices tiles,
        pos /= start,
        let tiles' = tiles A.// [(pos, True)],
        let cycle@(s, cyc) = T.detectCycle (path tiles' start direction),
        (not . null) cyc
    ]
