{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.List (foldl')
import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as M
import Data.Set (Set)
import qualified Data.Set as S
import Data.Text (pack)
import Text.Pretty.Simple (OutputOptions (..), defaultOutputOptionsNoColor, pPrintOpt)
import Text.Pretty.Simple.Internal.Printer (CheckColorTty (..))
import qualified Tools as T

pretty :: (Show a) => a -> IO ()
pretty =
  pPrintOpt
    NoCheckColorTty
    defaultOutputOptionsNoColor
      { outputOptionsCompact = True
      }

main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "Parsed:"
  pretty input
  putStrLn "==============================="

  putStrLn "Part A: "
  pretty (partA input)
  putStrLn ""

  putStrLn "Part B: "
  pretty (partB input)

------------ [ Input ] ---------------
type Input = [(Int, [Int])]

------------ [ Parse ] ---------------
runParser :: Parser Input -> String -> IO Input
runParser p s = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

parseInput :: String -> IO Input
parseInput = runParser $ parseEquation `P.sepBy1` P.endOfLine
  where
    parseEquation = do
      val <- P.decimal
      P.string ": "
      nums <- P.decimal `P.sepBy1` P.char ' '
      return (val, nums)

------------ [ Part A ] ---------------
partA eqs =
  sum
    [ val
      | (val, eq) <- eqs,
        val `elem` possibilities eq ops
    ]
  where
    ops 0 = [[]]
    ops n = concat [map (op :) (ops (n - 1)) | op <- [(+), (*)]]

possibilities :: [Int] -> (Int -> [[Int -> Int -> Int]]) -> [Int]
possibilities eq ops =
  [ foldl'
      (\acc (op, v) -> acc `op` v)
      (head eq)
      (zip op (drop 1 eq))
    | op <- ops (length eq - 1)
  ]

------------ [ Part B ] ---------------
partB eqs =
  sum
    [ val
      | (val, eq) <- eqs,
        val `elem` possibilities eq ops
    ]
  where
    ops 0 = [[]]
    ops n = concat [map (op :) (ops (n - 1)) | op <- [(+), (*), conct]]

    conct x y = read (show x ++ show y)
