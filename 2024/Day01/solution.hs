{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (Parser)
import Data.Attoparsec.Text qualified as P
import Data.Text (pack)

import Data.Map.Lazy (Map)
import Data.Map.Lazy qualified as M
import Data.Set (Set)
import Data.Set qualified as S

import Data.List (sort)
import Tools qualified as T

main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)

------------ [ Input ] ---------------
type Input = ([Int], [Int])

------------ [ Parse ] ---------------
runParser :: Parser Input -> String -> IO Input
runParser p s = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

parseInput :: String -> IO Input
parseInput = runParser $ unzip <$> parseLine `P.sepBy1` P.char '\n'

parseLine :: Parser (Int, Int)
parseLine = do
  left <- P.many1 P.digit
  P.many1 P.space
  right <- P.many1 P.digit
  pure (read left, read right)

------------ [ Part A ] ---------------
partA (left, right) =
  sum . map abs $
    zipWith (-) (sort left) (sort right)

------------ [ Part B ] ---------------
partB (left, right) =
  sum . map (uncurry (*)) $
    countAppearances (sort left) (sort right)

countAppearances :: [Int] -> [Int] -> [(Int, Int)]
countAppearances [] _ = []
countAppearances _ [] = []
countAppearances lss@(l : ls) rss@(r : rs)
  | l == r = (l, count) : countAppearances ls rest
  | l < r = countAppearances ls rss
  | l > r = countAppearances lss rs
 where
  (same, rest) = span (== l) rss
  count = length same
