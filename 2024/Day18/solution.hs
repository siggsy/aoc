{-# LANGUAGE OverloadedStrings #-}

import Data.Array.IArray (Array, listArray, (!))
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.Maybe (fromMaybe)
import qualified Data.Set as S
import Debug.Trace (trace)
import System.Environment (getArgs)
import Tools (Coordinate (..))
import qualified Tools as T

------------ [ Types ] ---------------
type Input = [Coordinate]

------------ [ Parse ] ---------------
parseInput :: String -> IO Input
parseInput = T.runParser $ ((:.) <$> P.decimal <*> (P.char ',' >> P.decimal)) `P.sepBy1'` P.endOfLine

------------ [ Main ] ----------------
main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "+---------------------------+"
  putStrLn "| Part A:                   |"
  putStrLn "+---------------------------+"
  partA input
  putStrLn ""

  putStrLn "+---------------------------+"
  putStrLn "| Part B:                   |"
  putStrLn "+---------------------------+"
  partB input

------------ [ Part A ] ---------------
partA :: Input -> IO ()
partA input = do
  let (path : _) = runDijkstra (0 :. 0, 70 :. 70) (take 1024 input)
  print $ length path - 1

runDijkstra :: (Coordinate, Coordinate) -> [Coordinate] -> [[Coordinate]]
runDijkstra bounds input =
  let end = snd bounds
      obstacles = S.fromList input
      paths =
        T.dijkstra
          (filter (`S.notMember` obstacles) . T.bounded bounds T.crossAround)
          (\v -> const 1)
          (== end)
          (0 :. 0)
   in paths

------------ [ Part B ] ---------------
partB :: Input -> IO ()
partB input = do
  print $ bisectBytes (0 :. 0, 70 :. 70) input

bisectBytes :: (Coordinate, Coordinate) -> [Coordinate] -> Coordinate
bisectBytes bounds input = go 0 (length input)
  where
    arr = listArray (0, length input - 1) input :: Array Int Coordinate
    go s e
      | s + 1 >= e = if isReachable n then arr ! n else arr ! (n - 1)
      | isReachable n = go (n + 1) e
      | otherwise = go s (n - 1)
      where
        n = ((e - s) `div` 2) + s
        isReachable n = not . null $ runDijkstra bounds (take n input)
