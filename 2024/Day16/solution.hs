{-# LANGUAGE DeriveGeneric #-}

import Control.Monad (void)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Representable.State (stateT)
import Control.Monad.Trans.Maybe (MaybeT (runMaybeT), hoistMaybe)
import Data.Array.IArray (Array, IArray (bounds), Ix (index, rangeSize), assocs, elems, (!), (!?), (//))
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.Bifunctor (second)
import Data.Hashable (Hashable)
import Data.List (foldl')
import Data.Maybe (listToMaybe)
import GHC.Generics (Generic)
import Tools (Coordinate (..))
import qualified Tools as T

------------ [ Types ] ---------------
type Input = Array Coordinate Char

data Direction = N | W | S | E deriving (Show, Eq, Ord, Enum, Generic)

instance Hashable Direction

-- >>> turnLeft W
-- S
turnLeft :: Direction -> Direction
turnLeft E = N
turnLeft s = succ s

-- >>> turnRight S
-- W
turnRight :: Direction -> Direction
turnRight N = E
turnRight s = pred s

-- >>> (0 :. 0) `towards` N
-- <0, -1>
towards :: Coordinate -> Direction -> Coordinate
towards (x :. y) d = case d of
  N -> x :. y - 1
  W -> x - 1 :. y
  S -> x :. y + 1
  E -> x + 1 :. y

------------ [ Parse ] ---------------
parseInput :: String -> IO Input
parseInput = pure . T.parseMap id

------------ [ Main ] ----------------
main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "-----------------------------"
  putStrLn "| Part A:                   |"
  putStrLn "-----------------------------"
  partA input
  putStrLn ""

  putStrLn "-----------------------------"
  putStrLn "| Part B:                   |"
  putStrLn "-----------------------------"
  partB input

------------ [ Part A ] ---------------
partA :: Array Coordinate Char -> IO ()
partA input = do
  let (path:_) = runDijkstra input
      mapWithPath = drawPath input path
  putStrLn $ T.showMap (bounds mapWithPath) (mapWithPath !)
  print $ score path

runDijkstra :: Input -> [[(Coordinate, Direction)]]
runDijkstra input =
  T.dijkstra
    options
    cost
    (\(pos, _) -> input ! pos == 'E')
    (startPos, E)
  where
    ((startPos, _) : _) = filter (\(_, t) -> t == 'S') (assocs input)

    maxRange = rangeSize (bounds input)
    stateToInt (pos, dir) = index (bounds input) pos + (maxRange + 1) * fromEnum dir

    options (pos, dir) =
      filter
        (\(p, _) -> input !? p /= Just '#')
        [ (pos `towards` dir, dir),
          (pos, turnLeft dir),
          (pos, turnRight dir)
        ]

    cost (pos, dir) (pos', dir')
      | dir == dir' = 1
      | otherwise = 1000

drawPath :: Array Coordinate Char -> [(Coordinate, Direction)] -> Array Coordinate Char
drawPath arr path = arr // map (second arrowTo) path
  where
    arrowTo N = '^'
    arrowTo W = '<'
    arrowTo S = 'v'
    arrowTo E = '>'

score :: [(Coordinate, Direction)] -> Int
score [_] = 0
score ((pos, dir) : p2@(pos', dir') : ps)
  | dir == dir' = 1 + score (p2 : ps)
  | otherwise = 1000 + score (p2 : ps)

------------ [ Part B ] ---------------
partB :: Input -> IO ()
partB input = do
  let paths = runDijkstra input
      mapped = foldl' drawPath' input paths
  putStrLn $ T.showMap (bounds mapped) (mapped !)
  print $ length . filter (== 'O') $ elems mapped

drawPath' :: Array Coordinate Char -> [(Coordinate, Direction)] -> Array Coordinate Char
drawPath' arr path = arr // map (second (const 'O')) path
