{-# LANGUAGE OverloadedStrings #-}

import Data.Array.IArray (Array, indices, listArray, (!), (!?))
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.List
import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as M
import Data.Set (Set)
import qualified Data.Set as S
import Data.Text (pack)
import Text.Pretty.Simple (OutputOptions (..), defaultOutputOptionsNoColor, pPrintOpt)
import Text.Pretty.Simple.Internal.Printer (CheckColorTty (..))
import qualified Tools as T

pretty :: (Show a) => a -> IO ()
pretty =
  pPrintOpt
    NoCheckColorTty
    defaultOutputOptionsNoColor
      { outputOptionsCompact = True
      }

main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "Parsed:"
  -- putStrLn (T.printArray input)
  putStrLn "==============================="

  putStrLn "Part A: "
  pretty (partA input)
  putStrLn ""

  putStrLn "Part B: "
  pretty (partB input)

------------ [ Input ] ---------------
type Input = Array Pos Char

type Pos = (Int, Int)

type PlantMap = Array Pos Char

------------ [ Parse ] ---------------
runParser :: Parser Input -> String -> IO Input
runParser p s = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

parseInput :: String -> IO Input
parseInput s = pure $ listArray ((0, 0), (y - 1, x - 1)) plants
  where
    plants = concat (lines s)

    y = length (lines s)
    x = (length . head) (lines s)

------------ [ Part A ] ---------------
partA = sum . map price . findRegions

around :: Pos -> [Pos]
around (y, x) = [(y - 1, x), (y, x + 1), (y + 1, x), (y, x - 1)]

expandToRegion :: PlantMap -> Char -> Pos -> Set Pos -> Set Pos
expandToRegion pMap plant pos@(y, x) explored
  | pos `S.member` explored = explored
  | pMap !? pos == Just plant =
      foldr
        (expandToRegion pMap plant)
        (S.insert pos explored)
        (around pos)
  | otherwise = explored

findRegions :: PlantMap -> [Set Pos]
findRegions pMap = go (S.fromList (indices pMap))
  where
    go :: Set Pos -> [Set Pos]
    go toExplore = case S.lookupMin toExplore of
      Just pos ->
        let region = expandToRegion pMap (pMap ! pos) pos S.empty
         in region : go (S.difference toExplore region)
      Nothing -> []

area :: Set Pos -> Int
area = S.size

perimiter :: Set Pos -> Int
perimiter = S.size . fences

price :: Set Pos -> Int
price region = area region * perimiter region

------------ [ Part B ] ---------------
partB = sum . map price' . findRegions

fences :: Set Pos -> Set (Pos, Pos)
fences region = go region
  where
    go toCheck = case S.minView toCheck of
      Just (pos, toCheck') ->
        let missingAround =
              S.difference
                (S.fromList (around pos))
                region
            posFences =
              S.mapMonotonic
                (,pos)
                missingAround
         in posFences `S.union` go toCheck'
      Nothing -> S.empty

sides :: Set Pos -> Int
sides region = go (fences region)
  where
    go fences = case S.lookupMin fences of
      Just fence ->
        1 + go (fences `S.difference` neg `S.difference` pos)
        where
          ((y, x), (y', x')) = fence
          (neg, pos) =
            if x == x'
              then (l,r)
              else (u,d)

          fencesTo = S.fromList . takeWhile (`S.member` fences)
          l = fencesTo [((y, x + dx), (y', x' + dx)) | dx <- [0, -1 ..]]
          r = fencesTo [((y, x + dx), (y', x' + dx)) | dx <- [0, 1 ..]]
          u = fencesTo [((y + dy, x), (y' + dy, x')) | dy <- [0, -1 ..]]
          d = fencesTo [((y + dy, x), (y' + dy, x')) | dy <- [0, 1 ..]]
      Nothing -> 0

price' :: Set Pos -> Int
price' region = area region * sides region
