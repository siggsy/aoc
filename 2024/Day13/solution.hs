{-# LANGUAGE OverloadedStrings #-}

import Data.Array.IArray (inRange)
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.Ix (Ix (..))
import Data.Maybe (mapMaybe)
import Tools (Coordinate (..))
import qualified Tools as T
import Data.Ratio
import GHC.Real (Ratio(..))
import Data.Tuple (swap)

------------ [ Types ] ---------------
type Input = [Prize]

data Prize = Prize
  { buttonA :: !(Int, Int),
    buttonB :: !(Int, Int),
    prize :: !(Int, Int)
  }
  deriving (Show)

------------ [ Parse ] ---------------
parseInput :: String -> IO Input
parseInput = T.runParser $ parsePrize `P.sepBy1'` P.endOfLine

parsePrize :: Parser Prize
parsePrize = do
  P.string "Button A: "
  a <- parseCommand

  P.string "Button B: "
  b <- parseCommand

  P.string "Prize: X="
  x <- P.decimal
  P.string ", Y="
  y <- P.decimal
  P.endOfLine
  return $ Prize a b (x, y)
  where
    parseCommand = do
      P.string "X+"
      x <- P.decimal
      P.string ", Y+"
      y <- P.decimal
      P.endOfLine
      pure (x, y)

------------ [ Main ] ----------------
main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "-----------------------------"
  putStrLn "| Part A:                   |"
  putStrLn "-----------------------------"
  partA input
  putStrLn ""

  putStrLn "-----------------------------"
  putStrLn "| Part B:                   |"
  putStrLn "-----------------------------"
  partB input

------------ [ Part A ] ---------------
partA :: Input -> IO ()
partA input = do
  print . sum . map cost . mapMaybe findPrize $ input

cost :: Coordinate -> Int
cost (a :. b) = a * 3 + b

------------ [ Part B ] ---------------
partB :: Input -> IO ()
partB input = do
  let input' = map fixPrize input
  print . sum . map cost . mapMaybe findPrize $ input'
  where
    fixPrize p =
      let (x, y) = prize p
       in p {prize = (10000000000000 + x, 10000000000000 + y)}


findPrize :: Prize -> Maybe Coordinate
findPrize (Prize a b (xp, yp)) =
  let ab = tryWith a b
      ba = swap (tryWith b a)
   in case (ab, ba) of
    ((x :% 1, y :% 1), (x' :% 1, y' :% 1))
      | cost (x :. y) < cost (x' :. y') -> Just (x :. y)
      | otherwise -> Just (x' :. y')
    ((x :% 1, y :% 1), _) -> Just (x :. y)
    (_, (x :% 1, y :% 1)) -> Just (x :. y)
    _notInteger -> Nothing
  where
    tryWith (xa, ya) (xb, yb) =
      let k1 = ya % xa
          k2 = yb % xb
          n = (yp % 1) - (xp % 1) * k1
          x = n / (k2 - k1)
          y = x * k2
       in (((yp % 1) - y) * (1 % ya) , y * (1 % yb))
