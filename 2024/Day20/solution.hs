{-# LANGUAGE OverloadedStrings #-}

import Data.Array.IArray (Array, assocs, (!), (!?))
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IM
import Data.List (find)
import qualified Data.Map.Strict as M
import Data.Maybe (catMaybes, fromJust, listToMaybe, mapMaybe)
import Retrie.PatternMap.Bag (Map (Map))
import Tools (Coordinate (..))
import qualified Tools as T
import Debug.Trace (trace)

------------ [ Types ] ---------------
type Input = Array Coordinate Char

------------ [ Parse ] ---------------
parseInput :: String -> IO Input
parseInput = pure . T.parseMap id

------------ [ Main ] ----------------
main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "+---------------------------+"
  putStrLn "| Part A:                   |"
  putStrLn "+---------------------------+"
  partA input
  putStrLn ""

  putStrLn "+---------------------------+"
  putStrLn "| Part B:                   |"
  putStrLn "+---------------------------+"
  partB input

------------ [ Part A ] ---------------
partA :: Input -> IO ()
partA input = do
  let cheats = findCheats input 2
  print $ sum . map snd . dropWhile (\(t, _) -> t < 100) $ IM.toAscList cheats

markTimes :: Input -> Maybe [(Coordinate, Int)]
markTimes arr = init <$> go [(start, 0)] 0 start
  where
    start = head (findTiles 'S' arr)
    end = head (findTiles 'E' arr)
    go mss@(m : ms) i pos
      | pos == end = Just ((pos, i) : mss)
      | otherwise =
          find (\c -> arr ! c /= '#' && c /= fst m) (T.crossAround pos)
            >>= go ((pos, i) : mss) (i + 1)

findCheats :: Input -> Int -> IntMap Int
findCheats arr n =
  go IM.empty (reverse . map fst $ times)
  where
    times = fromJust $ markTimes arr
    timeMap = M.fromList times
    start = head (findTiles 'S' arr)
    end = head (findTiles 'E' arr)

    diffTime pos1 pos2 =
      let t1 = timeMap M.! pos1
          t2 = timeMap M.! pos2
       in if (t2 - t1) > dist
            then return $ (t2 - t1) - dist
            else fail "Not faster"
      where
        dist = pos1 `manhattan` pos2

    manhattan (x :. y) (a :. b) = abs (x - a) + abs (y - b)

    go :: IntMap Int -> [Coordinate] -> IntMap Int
    go cheatMap [] = cheatMap
    go cheatMap (t : ts) =
      let options =
            mapMaybe (t `diffTime`)
              . filter (\t' -> t `manhattan` t' <= n)
              $ ts
          cheatMap' =
            IM.unionWith
              (+)
              (IM.fromListWith (+) . map (,1) $ options)
              cheatMap
       in go cheatMap' ts

findTiles :: Char -> Input -> [Coordinate]
findTiles t =
  map fst
    . filter (\(_, t') -> t' == t)
    . assocs

------------ [ Part B ] ---------------
partB :: Input -> IO ()
partB input = do
  let cheats = findCheats input 20
  print $ sum . map snd . dropWhile (\(t, _) -> t < 100) $ IM.toAscList cheats
