module Tools where

import Data.Array qualified as A
import Data.Array.IArray (Array, listArray, (!))
import Data.Attoparsec.Text (Parser)
import Data.Attoparsec.Text qualified as P
import Data.Heap (Entry (..), Heap)
import Data.Heap qualified as H
import Data.IntMap (IntMap)
import Data.IntMap qualified as IM
import Data.IntSet (IntSet)
import Data.IntSet qualified as IS
import Data.List.Extra (intersperse, merge)
import Data.Map qualified as M
import Data.Maybe (mapMaybe)
import Data.Semiring (IntMapOf (IntMapOf))
import Data.Set (Set)
import Data.Text (pack)
import Data.Tuple (swap)
import GHC.Ix (Ix, inRange, index, range, unsafeIndex)
import Text.Pretty.Simple (OutputOptions (..), defaultOutputOptionsNoColor, pPrintOpt)
import Text.Pretty.Simple.Internal.Printer (CheckColorTty (..))

-------------------------- [ Input helpers ] ------------------------------------

pretty :: (Show a) => a -> IO ()
pretty =
  pPrintOpt
    NoCheckColorTty
    defaultOutputOptionsNoColor
      { outputOptionsCompact = True
      }

runParser :: Parser a -> String -> IO a
runParser p s = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

-------------------------- [ Dealing with maps ] --------------------------------

infix 0 :.

data Coordinate = !Int :. !Int
  deriving (Eq)

instance Show Coordinate where
  showsPrec _ (x :. y) = showChar '(' . shows x . showString ", " . shows y . showChar ')'

instance Ord Coordinate where
  compare (x :. y) (a :. b) = compare (y, x) (b, a)

instance Ix Coordinate where
  range (x :. y, a :. b) = map (uncurry (:.) . swap) (range ((y, x), (b, a)))
  inRange (x :. y, a :. b) (i :. j) = inRange ((y, x), (b, a)) (j, i)
  index (x :. y, a :. b) (i :. j) = index ((y, x), (b, a)) (j, i)

parseMap :: (Char -> a) -> String -> Array Coordinate a
parseMap f s = listArray (0 :. 0, x - 1 :. y - 1) m
 where
  m = (map f . concat) (lines s)
  x = (length . head) (lines s)
  y = length (lines s)

crossAround :: Coordinate -> [Coordinate]
crossAround (x :. y) =
  [ x :. y - 1
  , x - 1 :. y
  , x + 1 :. y
  , x :. y + 1
  ]

xAround :: Coordinate -> [Coordinate]
xAround (x :. y) =
  [ x - 1 :. y - 1
  , x + 1 :. y - 1
  , x - 1 :. y + 1
  , x + 1 :. y + 1
  ]

starAround :: Coordinate -> [Coordinate]
starAround coor = merge (crossAround coor) (xAround coor)

bounded :: (Coordinate, Coordinate) -> (Coordinate -> [Coordinate]) -> Coordinate -> [Coordinate]
bounded bounds f = filter (A.inRange bounds) . f

reachable :: (Coordinate, Coordinate) -> (Coordinate -> [Coordinate]) -> Coordinate -> [Coordinate]
reachable bounds next curr = map toCoordinate (IS.toList indices)
 where
  toIndex = unsafeIndex bounds
  indices = go IS.empty curr

  (sx :. _, ex :. _) = bounds

  toCoordinate :: Int -> Coordinate
  toCoordinate i =
    let dx = (ex - sx)
     in i `mod` dx :. i `div` dx

  go :: IntSet -> Coordinate -> IntSet
  go visited pos
    | toIndex pos `IS.member` visited = visited
    | otherwise =
        foldr
          (flip go)
          (IS.insert (toIndex pos) visited)
          (filter (inRange bounds) (next pos))

dijkstra ::
  (Ord v) =>
  (v -> Int) -> -- Index function
  (v -> [v]) -> -- Neighbours function
  (v -> v -> Int) -> -- Cost function
  (v -> Bool) -> -- Set of ends
  v -> -- Start
  Maybe [v]
dijkstra index neighbs cost ends start =
  search
    (IM.singleton (index start) 0)
    (H.singleton (Entry 0 start))
    IM.empty
 where
  constructPath u prevMap = case prevMap IM.!? index u of
    Just v -> u : constructPath v prevMap
    Nothing -> [u]

  search distance queue prevMap = case H.viewMin queue of
    Nothing -> Nothing
    Just (Entry c u, queue')
      | ends u -> Just $ constructPath u prevMap
      | otherwise -> search distance' queue'' prevMap'
     where
      vertexIndex = index u
      queue'' = queue' `H.union` (H.fromList . map (uncurry Entry)) queued
      queued = mapMaybe neighbCost (neighbs u)
      distance' = IM.unionWith const (IM.fromList (map (\(c, v) -> (index v, c)) queued)) distance
      prevMap' = IM.unionWith const (IM.fromList (map (\(c, v) -> (index v, u)) queued)) prevMap

      neighbCost v = case distance IM.!? index v of
        Just d
          | alt < d -> Just (alt, v)
          | otherwise -> Nothing
        Nothing -> Just (alt, v)
       where
        alt = c + cost u v

showMap :: (a -> Char) -> Array Coordinate a -> String
showMap f arr = unlines arrayLines
 where
  (sx :. sy, ex :. ey) = A.bounds arr
  arrayLines =
    [ intersperse
      ' '
      [ f (arr ! (x :. y))
      | x <- [sx .. ex]
      ]
    | y <- [sy .. ey]
    ]

-------------------------- [ There always has to be a cycle ] -------------------

detectCycle :: (Eq a) => [a] -> ([a], [a])
detectCycle as = cycle as as
 where
  cycle (x : xs) (_ : y : ys)
    | x == y = start as xs
    | otherwise = cycle xs ys
  cycle _ _ = (as, [])

  start (x : xs) (y : ys)
    | x == y = ([], x : cyc x xs)
    | otherwise = let (xs', ys') = start xs ys in (x : xs', ys')

  cyc x (y : ys)
    | x == y = []
    | otherwise = y : cyc x ys
