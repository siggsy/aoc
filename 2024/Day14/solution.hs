{-# LANGUAGE OverloadedStrings #-}

import Control.Concurrent (threadDelay)
import Control.Monad (when)
import Data.Array (Ix (range))
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.List (partition)
import qualified Data.Set as S
import Text.Printf (printf)
import Tools (Coordinate (..))
import qualified Tools as T

------------ [ Types ] ---------------
type Input = [Robot]

type Robot = (Coordinate, Coordinate)

------------ [ Parse ] ---------------
parseInput :: String -> IO Input
parseInput = T.runParser $ parseRobot `P.sepBy1'` P.endOfLine
  where
    parseRobot :: Parser Robot
    parseRobot = do
      P.string "p="
      pos <- parseCoordinate
      P.string " v="
      vel <- parseCoordinate
      return (pos, vel)

    parseCoordinate = (:.) <$> P.signed P.decimal <*> (P.char ',' >> P.signed P.decimal)

------------ [ Main ] ----------------
main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "+---------------------------+"
  putStrLn "| Part A:                   |"
  putStrLn "+---------------------------+"
  partA input
  putStrLn ""

  putStrLn "+---------------------------+"
  putStrLn "| Part B:                   |"
  putStrLn "+---------------------------+"
  partB input

------------ [ Part A ] ---------------
partA :: Input -> IO ()
partA input = do
  let bound = 101 :. 103
      finalPos = map (posAfter bound 100) input
  print (safetyFactor bound finalPos)

posAfter :: Coordinate -> Int -> Robot -> Coordinate
posAfter (ex :. ey) n (x :. y, vx :. vy) = (x + vx * n) `mod` ex :. (y + vy * n) `mod` ey

quadrants :: Coordinate -> [Coordinate] -> [[Coordinate]]
quadrants (ex :. ey) coords = [firstQuadrant, secondQuadrant, thirdQuadrant, fourthQuadrant]
  where
    halfX = ex `div` 2
    halfY = ey `div` 2
    leftRight (x :. y) = x < halfX
    topBottom (x :. y) = y < halfY
    middle (x :. y) = x == halfX || y == halfY
    coords' = filter (not . middle) coords

    (top, bottom) = partition topBottom coords'

    (firstQuadrant, secondQuadrant) = partition leftRight top
    (thirdQuadrant, fourthQuadrant) = partition leftRight bottom

safetyFactor :: Coordinate -> [Coordinate] -> Int
safetyFactor (ex :. ey) = product . map length . quadrants (ex :. ey)

------------ [ Part B ] ---------------
partB :: Input -> IO ()
partB input = do
  let bound = 101 :. 103
      actualSize = length input
      (n, tree) =
        head
          [(n, final)
             | n <- [0 .. ],
               let final = map (posAfter bound n) input,
               S.size (S.fromList final) == actualSize
             ]
  printf "Number: %d\n" n
  putStrLn (showPos bound tree)

showPos :: Coordinate -> [Coordinate] -> String
showPos (ex :. ey) coords = unlines [[toTile (x :. y) | x <- [0 .. ex - 1]] | y <- [0 .. ey - 1]]
  where
    scoords = S.fromList coords
    toTile pos
      | pos `S.member` scoords = '#'
      | otherwise = '.'
