{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as M
import Data.Set (Set)
import qualified Data.Set as S
import Data.Text (pack)
import Text.Pretty.Simple (OutputOptions (..), defaultOutputOptionsNoColor, pPrintOpt)
import Text.Pretty.Simple.Internal.Printer (CheckColorTty (..))
import qualified Tools as T
import Control.Monad.Memo (Memo, MonadMemo (memo), for2, startEvalMemo)

pretty :: (Show a) => a -> IO ()
pretty =
  pPrintOpt
    NoCheckColorTty
    defaultOutputOptionsNoColor
      { outputOptionsCompact = True
      }

main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "Parsed:"
  pretty input
  putStrLn "==============================="

  putStrLn "Part A: "
  pretty (partA input)
  putStrLn ""

  putStrLn "Part B: "
  pretty (partB input)

------------ [ Input ] ---------------
type Input = [Int]

------------ [ Parse ] ---------------
runParser :: Parser Input -> String -> IO Input
runParser p s = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

parseInput :: String -> IO Input
parseInput = pure . map read . words . head . lines

------------ [ Part A ] ---------------
partA = sum . startEvalMemo . mapM (expand 25)

expand :: Int -> Int -> Memo (Int, Int) Int Int
expand 0 i = return 1
expand n 0 = for2 memo expand (n-1) 1
expand n i = case split i of
  Just (x,y) -> (+) <$> for2 memo expand (n-1) x <*> for2 memo expand (n-1) y
  Nothing -> for2 memo expand (n-1) (i*2024)

split :: Int -> Maybe (Int, Int)
split i
  | even (length num) = Just ((read . take half) num, (read . drop half) num)
  | otherwise = Nothing
  where
    num = show i
    half = length num `div` 2

------------ [ Part B ] ---------------
partB = sum . startEvalMemo . mapM (expand 75)
