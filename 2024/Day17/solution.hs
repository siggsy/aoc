{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.Trans.Writer (Writer, runWriter, tell)
import Data.Array.IArray (Array, bounds, inRange, listArray, (!))
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.Bits (Bits (xor))
import Data.List (genericLength)
import qualified Tools as T
import Debug.Trace (trace)
import Text.Printf (printf)

------------ [ Types ] ---------------
type Input = (Computer, Program)

type Program = [Int]

data Computer = Computer
  { regA :: !Integer,
    regB :: !Integer,
    regC :: !Integer,
    ip :: !Integer
  }
  deriving (Show)

------------ [ Parse ] ---------------
parseInput :: String -> IO Input
parseInput = T.runParser $ (,) <$> parseComputer <*> parseProgram
  where
    parseComputer = do
      P.string "Register A: "
      regA <- P.decimal
      P.endOfLine
      P.string "Register B: "
      regB <- P.decimal
      P.endOfLine
      P.string "Register C: "
      regC <- P.decimal
      P.endOfLine
      P.endOfLine
      return (Computer regA regB regC 0)

    parseProgram = do
      P.string "Program: "
      p <- P.decimal `P.sepBy1` P.char ','
      P.endOfLine
      return p

------------ [ Main ] ----------------
main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "-----------------------------"
  putStrLn "| Part A:                   |"
  putStrLn "-----------------------------"
  partA input
  putStrLn ""

  putStrLn "-----------------------------"
  putStrLn "| Part B:                   |"
  putStrLn "-----------------------------"
  partB input

------------ [ Part A ] ---------------
partA :: Input -> IO ()
partA (computer, prog) = do
  let (comp, out) = runProgram computer prog
  T.pretty comp
  print out

combo :: Computer -> Int -> Integer
combo comp op = case op of
  4 -> regA comp
  5 -> regB comp
  6 -> regC comp
  7 -> error "usage of reserved operand 7"
  x | x >= 0 && x <= 3 -> toInteger x
  _invalid -> error "operand not a 3-bit number"

runInstruction :: Computer -> Int -> Int -> Writer [Int] Computer
runInstruction comp 0 op =
  -- adv
  return $ comp {regA = regA comp `div` 2 ^ combo comp op, ip = ip comp + 2}
runInstruction comp 1 op =
  -- bxl
  return $ comp {regB = regB comp `xor` toInteger op, ip = ip comp + 2}
runInstruction comp 2 op =
  -- bst
  return $ comp {regB = toInteger (combo comp op) `mod` 8, ip = ip comp + 2}
runInstruction comp 3 op -- jnz
  | regA comp == 0 = return $ comp {ip = ip comp + 2}
  | otherwise = return $ comp {ip = toInteger op}
runInstruction comp 4 op = return $ comp {regB = regB comp `xor` regC comp, ip = ip comp + 2}
runInstruction comp 5 op = do
  -- out
  tell [fromInteger (combo comp op `mod` 8)]
  return $ comp {ip = ip comp + 2}
runInstruction comp 6 op =
  -- bdv
  return $ comp {regB = regA comp `div` 2 ^ combo comp op, ip = ip comp + 2}
runInstruction comp 7 op =
  -- cdv
  return $ comp {regC = regA comp `div` 2 ^ combo comp op, ip = ip comp + 2}

runProgram :: Computer -> Program -> (Computer, [Int])
runProgram comp ps = runWriter (go comp)
  where
    prog = listArray (0, genericLength ps - 1) ps :: Array Integer Int
    go comp = do
      let i = ip comp
      if inRange (bounds prog) (i + 1)
        then do
          let ins = prog ! i
              op = prog ! (i + 1)
          comp' <- runInstruction comp ins op
          go comp'
        else return comp

------------ [ Part B ] ---------------
partB :: Input -> IO ()
partB (computer, program) = do
  let (regA:_) = findRegA computer program
      (_, out) = runProgram (computer { regA = regA }) program
  printf "RegA: %d\n" regA
  printf "Program: %s\n" (show program)
  printf "Output:  %s\n" (show out)
  printf "IsEqual: %s\n" $ show $ out == program

-- Program loops over regA /= 8
-- We find the solution by "looping backwards" and choosing candidates for solution
-- We go from O(8^n) -> O(8n) (if we only care about the first solution)
findRegA :: Computer -> Program -> [Integer]
findRegA computer program = go (0, 7) (reverse program)
  where
    go (start, end) [] = [start `div` 8]
    go (start, end) (p : ps) =
      concat
        [ go (regA' * 8, regA' * 8 + 7) ps
          | regA' <- [start .. end],
            let (_, o:_) = runProgram (computer {regA = regA'}) program,
            o == p
        ]
