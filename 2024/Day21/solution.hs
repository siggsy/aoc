import Control.Applicative ((<|>))
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.Maybe (fromMaybe, maybeToList)
import Tools (Coordinate (..))
import qualified Tools as T
import Data.List (minimumBy)
import Data.Function (on)
import Control.Monad.Memo (Memo, for2, memo, foldM, startEvalMemo)
import Control.Monad.Memo.Class (MonadMemo)

------------ [ Types ] ---------------
type Input = [String]

------------ [ Parse ] ---------------
parseInput :: String -> IO Input
parseInput = pure . lines

------------ [ Main ] ----------------
main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "+---------------------------+"
  putStrLn "| Part A:                   |"
  putStrLn "+---------------------------+"
  partA input
  putStrLn ""

  putStrLn "+---------------------------+"
  putStrLn "| Part B:                   |"
  putStrLn "+---------------------------+"
  partB input

------------ [ Part A ] ---------------
partA :: Input -> IO ()
partA input = do
  let numBot = delegateNum <$> input
      finalBot = map (delegate' 2) <$> numBot
  print $ sums input (minimum <$> finalBot)

sums :: Input -> [Int] -> Int
sums input = sum . zipWith (*) (map (read . init) input)

vector :: Coordinate -> Coordinate -> Coordinate
vector (x :. y) (a :. b) = a - x :. b - y

delegateNum :: [Char] -> [[Char]]
delegateNum = map concat . options . map (vectorToArrows True) . toVector numeric . ('A' : )

delegate :: [Char] -> [[String]]
delegate = options . map (vectorToArrows False) . toVector directional . ('A' : )

-- >>> vectorToArrows True ('A', (-2 :. 2))
-- ["^^<<A"]
vectorToArrows :: Bool -> (Char, Coordinate) -> [String]
vectorToArrows isNumeric (v, x :. y) =
  (++ ['A']) <$> filter (valid (position v)) (combine xs ys)
  where
    xPos = case compare x 0 of
      LT -> "<"
      EQ -> ""
      GT -> ">"
    yPos = case compare y 0 of
      LT -> "v"
      EQ -> ""
      GT -> "^"
    xs = concat (replicate (abs x) xPos)
    ys = concat (replicate (abs y) yPos)

    position v
      | isNumeric = numeric v
      | otherwise = directional v

    valid pos ps = case ps of
      [] -> inside pos
      (p:ps) -> inside pos && valid (pos `plus` p) ps

    (x :. y) `plus` p = case p of
      '<' -> x - 1 :. y
      '>' -> x + 1 :. y
      '^' -> x :. y + 1
      'v' -> x :. y - 1

    inside pos
      | isNumeric = pos /= (0 :. 0)
      | otherwise = pos /= (0 :. 1)


-- >>> options [["1"], ["32","23"], ["4"]]
-- [["1","32","4"],["1","23","4"]]
options :: [[a]] -> [[a]]
options [] = [[]]
options (as:ass) = concat [ (a :) <$> options ass | a <- as ]

-- >>> combine "<<" "^^"
-- ["<<^^","^^<<"]
combine :: String -> String -> [String]
combine [] s2 = [s2]
combine s1 [] = [s1]
combine s1 s2 = [s1 ++ s2, s2 ++ s1]

numeric :: Char -> Coordinate
numeric 'A' = 2 :. 0
numeric '0' = 1 :. 0
numeric '1' = 0 :. 1
numeric '2' = 1 :. 1
numeric '3' = 2 :. 1
numeric '4' = 0 :. 2
numeric '5' = 1 :. 2
numeric '6' = 2 :. 2
numeric '7' = 0 :. 3
numeric '8' = 1 :. 3
numeric '9' = 2 :. 3

directional :: Char -> Coordinate
directional '<' = 0 :. 0
directional 'v' = 1 :. 0
directional '>' = 2 :. 0
directional '^' = 1 :. 1
directional 'A' = 2 :. 1

toVector :: (Char -> Coordinate) -> String -> [(Char, Coordinate)]
toVector f [_] = []
toVector f (d1 : d2 : ds) =
  (d1, vector (f d1) (f d2)) : toVector f (d2 : ds)

------------ [ Part B ] ---------------
partB :: Input -> IO ()
partB input = do
  let numBot = delegateNum <$> input
      firstBot = map (delegate' 25) <$> numBot
      mins = minimum <$> firstBot

  print $ sums input mins

delegate' :: Int -> String -> Int
delegate' n cmds = startEvalMemo (go n cmds)
  where
    go 0 cmds = return (length cmds)
    go n cmds = minimum <$> traverse (fmap sum . traverse (for2 memo go (n - 1))) (delegate cmds)
