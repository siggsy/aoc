{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.Set (Set)
import qualified Data.Set as S
import Data.Text (pack)
import Text.Pretty.Simple (CheckColorTty (CheckColorTty), OutputOptions (..), defaultOutputOptionsNoColor, pPrint, pPrintOpt)
import qualified Tools as T

main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  pPrintOpt CheckColorTty (defaultOutputOptionsNoColor {outputOptionsCompact = True}) (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)

------------ [ Input ] ---------------
type Rule = (Int, Int)

type Update = [Int]

type Input = ([Rule], [Update])

------------ [ Parse ] ---------------
runParser :: Parser Input -> String -> IO Input
runParser p s = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

parseInput :: String -> IO Input
parseInput = runParser $ do
  rules <- parseRules
  P.endOfLine
  updates <- parseUpdates
  return (rules, updates)

parseRules :: Parser [Rule]
parseRules = P.many1 parseRule
  where
    parseRule = do
      x <- P.decimal
      P.char '|'
      y <- P.decimal
      P.endOfLine
      return (x, y)

parseUpdates :: Parser [Update]
parseUpdates = parseUpdate `P.sepBy1` P.endOfLine
  where
    parseUpdate = P.decimal `P.sepBy1` P.char ','

------------ [ Part A ] ---------------
partA (rules, updates) =
  sum
    [ update !! (length update `div` 2)
      | update <- updates,
        checkOrder (S.fromList rules) update
    ]

checkOrder :: Set Rule -> Update -> Bool
checkOrder rules = S.null . violated rules

violated :: Set Rule -> Update -> Set Rule
violated rules [] = S.empty
violated rules (x : us) =
  S.union
    (S.intersection rules (S.fromList (map (,x) us)))
    (violated rules us)

------------ [ Part B ] ---------------
partB (rules, updates) =
  sum
    [ update' !! (length update' `div` 2)
      | update <- updates,
        let update' = fixOrder (S.fromList rules) update,
        update' /= update
    ]

fixOrder :: Set Rule -> Update -> Update
fixOrder rules update
  | null toFix = update
  | otherwise = fixOrder rules (map applyFix update)
  where
    toFix = S.toList (violated rules update)
    applyFix = go toFix
    go [] p = p
    go ((x, y) : fs) p
      | p == x = y
      | p == y = x
      | otherwise = p
