{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (Parser)
import Data.Attoparsec.Text qualified as P
import Data.Text (pack)

import Data.Map.Lazy (Map)
import Data.Map.Lazy qualified as M
import Data.Set (Set)
import Data.Set qualified as S

import Tools qualified as T

main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)

------------ [ Input ] ---------------
type Input = [[Int]]

------------ [ Parse ] ---------------
runParser :: Parser Input -> String -> IO Input
runParser p s = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

parseInput :: String -> IO Input
parseInput = pure . map (map read . words) . lines

------------ [ Part A ] ---------------
partA = length . filter isSafe

isSafe :: [Int] -> Bool
isSafe xs = all _isSafe diffs
 where
  diffs = zipWith (-) xs (drop 1 xs)
  signs = map signum diffs
  direction = signum (sum signs)
  _isSafe x = (abs x <= 3) && (abs x >= 1) && signum x == direction

------------ [ Part B ] ---------------
partB = length . filter isSafeDampened

isSafeDampened :: [Int] -> Bool
isSafeDampened xs = isSafe xs || any isSafe dropped
 where
  dropped = [take i xs ++ drop (i + 1) xs | i <- [0 .. length xs - 1]]
