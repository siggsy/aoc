{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.List (intercalate, sortOn)
import Data.List.Extra (nubOrd)
import qualified Data.Map.Strict as M
import Data.Maybe (mapMaybe)
import Data.Set (Set)
import qualified Data.Set as S
import Debug.Trace (trace)
import qualified Tools as T

------------ [ Types ] ---------------
type Input = [(String, String)]

------------ [ Parse ] ---------------
parseInput :: String -> IO Input
parseInput = T.runParser $ connection `P.sepBy1'` P.endOfLine
  where
    connection =
      (,)
        <$> P.many1' P.letter
        <*> (P.char '-' >> P.many1' P.letter)

------------ [ Main ] ----------------
main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "+---------------------------+"
  putStrLn "| Part A:                   |"
  putStrLn "+---------------------------+"
  partA input
  putStrLn ""

  putStrLn "+---------------------------+"
  putStrLn "| Part B:                   |"
  putStrLn "+---------------------------+"
  partB input

------------ [ Part A ] ---------------
partA :: Input -> IO ()
partA input = do
  let edges = S.fromList . normalize $ input
      candidates = map (\(x, y) -> S.fromList [x, y]) . filter hasT $ input
  T.pretty $ length . cliques edges $ candidates

edge :: (Ord a) => a -> a -> (a, a)
edge u v = (min u v, max u v)

hasT :: (String, String) -> Bool
hasT ('t' : _, _) = True
hasT (_, 't' : _) = True
hasT _ = False

normalize :: Input -> Input
normalize = map (uncurry edge)

cliques :: Set (String, String) -> [Set String] -> [Set String]
cliques edges = nubOrd . go
  where
    tryJoin a b
      | S.size diff == 2 && S.member (edge u v) edges = Just (a <> diff)
      | otherwise = Nothing
      where
        diff = (a S.\\ b) <> (b S.\\ a)
        [v, u] = S.toList diff

    go [] = []
    go (c : cs) = mapMaybe (tryJoin c) cs <> go cs

------------ [ Part B ] ---------------
partB :: Input -> IO ()
partB input =
  putStrLn
    $ intercalate ","
      . S.toList
      . maxClique
    $ S.fromList . normalize
    $ input

maxClique :: Set (String, String) -> Set String
maxClique edges =
  let vertexMap =
        M.fromListWith (<>)
          . concatMap (\(x, y) -> [(x, S.singleton y), (y, S.singleton x)])
          $ S.toList edges
   in go (sortOn (S.size . snd) (M.assocs vertexMap)) S.empty
  where
    go :: [(String, Set String)] -> Set String -> Set String
    go [] cliq = cliq
    go ((v, us) : vm) cliq =
      let with = go (filter ((`S.member` us) . fst) vm) (S.insert v cliq)
          without = go vm cliq
       in if S.size with > S.size without
            then with
            else without
