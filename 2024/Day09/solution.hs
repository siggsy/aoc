{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.Char (digitToInt)
import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as M
import Data.Set (Set)
import qualified Data.Set as S
import Data.Text (pack)
import Text.Pretty.Simple (OutputOptions (..), defaultOutputOptionsNoColor, pPrintOpt)
import Text.Pretty.Simple.Internal.Printer (CheckColorTty (..))
import qualified Tools as T

pretty :: (Show a) => a -> IO ()
pretty =
  pPrintOpt
    NoCheckColorTty
    defaultOutputOptionsNoColor
      { outputOptionsCompact = True
      }

main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "Parsed:"
  pretty input
  putStrLn "==============================="

  putStrLn "Part A: "
  pretty (partA input)
  putStrLn ""

  putStrLn "Part B: "
  pretty (partB input)

------------ [ Input ] ---------------
type Input = [Int]

------------ [ Parse ] ---------------
runParser :: Parser Input -> String -> IO Input
runParser p s = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

parseInput :: String -> IO Input
parseInput s = pure $ map digitToInt (head . lines $ s)

------------ [ Part A ] ---------------
partA = checksum . compact

freeSpace :: [Int] -> Int
freeSpace [] = 0
freeSpace [_] = 0
freeSpace (b : f : dm) = f + freeSpace dm

blocks :: [Int] -> Int
blocks [] = 0
blocks [b] = b
blocks (b : _ : dm) = b + blocks dm

readDiskMap :: [Int] -> [Int]
readDiskMap = readDiskMap' 0
  where
    readDiskMap' _ [] = []
    readDiskMap' id [b] = replicate b id
    readDiskMap' id (b : f : dm) = replicate b id ++ replicate f (-1) ++ readDiskMap' (id + 1) dm

compact :: [Int] -> [Int]
compact dm =
  fillFree
    (take (blocks dm) disk)
    ((reverse . filter (/= -1)) disk)
  where
    disk = readDiskMap dm

    fillFree [] _ = []
    fillFree (-1 : ds) (r : rs) = r : fillFree ds rs
    fillFree (d : ds) rs = d : fillFree ds rs

checksum :: [Int] -> Int
checksum = sum . zipWith (\x y -> if y == -1 then 0 else x * y) [0 ..]

------------ [ Part B ] ---------------
partB = checksum . moveFiles

analyzeDiskMap :: [Int] -> [(Int, Int)]
analyzeDiskMap = go 0
  where
    go _ [] = []
    go id [b] = [(id, b)]
    go id (b : f : dm) = (id, b) : (-1, f) : go (id + 1) dm

moveFiles :: [Int] -> [Int]
moveFiles dm = expand disk'
  where
    disk = analyzeDiskMap dm
    files = filter ((/= -1) . fst) disk
    disk' = fillFree disk [] (reverse files)

    moveFile :: [(Int, Int)] -> (Int, Int) -> Maybe [(Int, Int)]
    moveFile [] _ = Nothing
    moveFile ((-1, free) : disk) (id, count)
      | free >= count = Just ((id, count) : (-1, free - count) : disk)
      | otherwise = ((-1, free) :) <$> moveFile disk (id, count)
    moveFile (f : disk) f'
      | f == f' = Nothing
      | otherwise = (f :) <$> moveFile disk f'

    fillFree :: [(Int, Int)] -> [Int] -> [(Int, Int)] -> [(Int, Int)]
    fillFree disk moved [] = removeMoved moved disk
    fillFree disk moved (f : files) =
      case moveFile disk f of
        Just disk' -> fillFree disk' (fst f : moved) files
        Nothing -> fillFree disk moved files

    removeMoved moved disk = reverse (go (reverse moved) (reverse disk))
      where
        go _ [] = []
        go [] disk = disk
        go moved@(id : files) (f@(id', size) : disk)
          | id == id' = (-1, size) : go files disk
          | otherwise = f : go moved disk

    expand [] = []
    expand ((id, size) : disk) = replicate size id ++ expand disk
