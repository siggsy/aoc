module Tools where

import qualified Data.Array as A
import Data.Array.IArray (Array, Ix (inRange, index, range), listArray, (!))
import Data.Attoparsec.Text (Parser)
import qualified Data.Attoparsec.Text as P
import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as M
import Data.Set (Set)
import qualified Data.Set as S
import Data.Text (pack)
import Data.Tuple (swap)
import Text.Pretty.Simple (OutputOptions (..), defaultOutputOptionsNoColor, pPrintOpt)
import Text.Pretty.Simple.Internal.Printer (CheckColorTty (..))

infix 0 :.

data Coordinate = !Int :. !Int deriving (Eq)

instance Show Coordinate where
  showsPrec n (x :. y) = showChar '(' . shows x . showString ", " . shows y . showChar ')'

instance Ord Coordinate where
  compare (x :. y) (a :. b) = compare (y, x) (b, a)

instance Ix Coordinate where
  range (x :. y, a :. b) = map (uncurry (:.) . swap) (range ((y, x), (b, a)))
  inRange (x :. y, a :. b) (i :. j) = inRange ((y, x), (b, a)) (j, i)
  index (x :. y, a :. b) (i :. j) = index ((y, x), (b, a)) (j, i)

parseMap :: (Char -> a) -> String -> Array Coordinate a
parseMap f s = listArray (0 :. 0, x - 1 :. y - 1) m
  where
    m = (map f . concat) (lines s)
    x = (length . head) (lines s)
    y = length (lines s)

crossAround :: Coordinate -> [Coordinate]
crossAround (x :. y) =
  [ x :. y - 1,
    x + 1 :. y,
    x :. y + 1,
    x - 1 :. y
  ]

xAround :: Coordinate -> [Coordinate]
xAround (x :. y) =
  [ x - 1 :. y - 1,
    x + 1 :. y - 1,
    x - 1 :. y + 1,
    x + 1 :. y + 1
  ]

starAround :: Coordinate -> [Coordinate]
starAround coor = crossAround coor ++ xAround coor

pretty :: (Show a) => a -> IO ()
pretty =
  pPrintOpt
    NoCheckColorTty
    defaultOutputOptionsNoColor
      { outputOptionsCompact = True
      }

runParser :: Parser a -> String -> IO a
runParser p s = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

detectCycle :: (Eq a) => [a] -> ([a], [a])
detectCycle as = cycle as as
  where
    cycle (x : xs) (_ : y : ys)
      | x == y = start as xs
      | otherwise = cycle xs ys
    cycle _ _ = (as, [])

    start (x : xs) (y : ys)
      | x == y = ([], x : cyc x xs)
      | otherwise = let (xs', ys') = start xs ys in (x : xs', ys')

    cyc x (y : ys)
      | x == y = []
      | otherwise = y : cyc x ys

dijkstra ::
  (Ord a, Ord b) =>
  ((a, b) -> [(a, b)]) ->
  Set b ->
  (a, b) ->
  Maybe [(a, b)]
dijkstra neighbs ends start = search S.empty (M.singleton start [start])
  where
    search visited next = case M.minViewWithKey next of
      Just (((cost, vertex), path), next')
        | vertex `S.member` ends -> Just path
        | vertex `S.member` visited -> search visited next'
        | otherwise -> search (S.insert vertex visited) next''
        where
          next'' = foldr (\n m -> M.insertWith (++) n [n] m) next' (neighbs (cost, vertex))
      _ -> Nothing

dijkstra' ::
  (Ord a, Ord b) =>
  ((a, b) -> [(a, b)]) ->
  Set b ->
  (a, b) ->
  Maybe (a, b)
dijkstra' neighbs ends start = head <$> dijkstra neighbs ends start

printArray :: (Show a) => Array (Int, Int) a -> String
printArray arr = unlines arrayLines
  where
    ((sy, sx), (ey, ex)) = A.bounds arr
    arrayLines =
      [ unwords
          [ show (arr ! (y, x))
            | x <- [sx .. ex]
          ]
        | y <- [sy .. ey]
      ]
