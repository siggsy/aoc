import Data.Attoparsec.Text (Parser, parseOnly)
import Data.Text (pack)


main :: IO ()
main = do
  input <- getContents >>= parse
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)


------------ [ Input ] ---------------
type Input = [Int]
type Output = Int

------------ [ Parse ] ---------------
parse :: String -> IO Input
parse s =
  case parseInput of
    Left  p ->
      case parseOnly p (pack s) of
        Left e  -> error ("Failed while parsing input: " ++ e)
        Right r -> pure r
    Right p -> pure (p s)

parseInput :: Either (Parser Input) (String -> Input)
parseInput = Left undefined


------------ [ Part A ] ---------------
partA :: Input -> Output
partA = undefined


------------ [ Part B ] ---------------
partB :: Input -> Output
partB = undefined
