module Days.Day14 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Attoparsec.Text
import Data.Void
{- ORMOLU_ENABLE -}

------------ PARSER ------------
inputParser :: Parser Input
inputParser = decimal `around` char ',' `sepBy` string " -> " `sepBy` endOfLine

------------ TYPES ------------
type Input = [[Pos]]

type OutputA = String

type OutputB = Void

data Material
  = Air
  | Sand
  | Rock

instance Show Material where
  show :: Material -> String
  show Air = "."
  show Sand = "o"
  show Rock = "#"

type Pos = (Int, Int)
type Sim = Map Pos Material

showSim :: Sim -> String
showSim sim = let
  (left, right, top, bottom) = U.mapBoundingBox sim
  in unlines . U.chunksOf (right-left) . concat $
    [ show material
    | x <- [left..right]
    , y <- [top..bottom]
    , let material = fromMaybe Air $ sim Map.!? (x,y)
    ]

line :: [Pos] -> Sim
line [] = Map.empty
line [p] = Map.empty
line ~((x,y):(x',y'):ps)
  = Map.unionWith const (line ((x',y'):ps)) 
  . Map.fromList
  $ zipWith
      (curry (, Rock))
      [x,x+signum (x'-x)..x']
      [y,y+signum (y'-y)..y']

createMap :: [[Pos]] -> Sim
createMap = Map.unionsWith const . map line

------------ PART A ------------
partA :: Input -> OutputA
partA = showSim . createMap

------------ PART B ------------
partB :: Input -> OutputB
partB = error "Not implemented yet!"

main :: IO ()
main = do
  input <- parse <$> readFile "input/Day14.txt"
  putStrLn (partA input)
  putStrLn (partB input)
