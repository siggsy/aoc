module Days.Day04 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import qualified Util.Util as U
import Util.Parsers

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text
import Data.Void
{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

type Range = (Int, Int)

range :: Range -> [Int]
range (a, a') = [a .. a']

empty :: [Int] -> Bool
empty [] = True
empty _ = False

intersectRange :: Range -> Range -> [Int]
intersectRange a b = range a `intersect` range b

isContained :: Range -> Range -> Bool
isContained a b = fst a <= fst b && snd a >= snd b

isAnyContained :: Range -> Range -> Bool
isAnyContained a b = isContained a b || isContained b a

------------ PARSER ------------
inputParser :: Parser Input
inputParser = ((decimal `around` char '-') `around` char ',') `sepBy` char '\n'

------------ TYPES ------------
type Input = [(Range, Range)]

type OutputA = Int

type OutputB = Int

------------ PART A ------------
partA :: Input -> OutputA
partA = sum . map (fromEnum . uncurry isAnyContained)

------------ PART B ------------
partB :: Input -> OutputB
partB = sum . map (fromEnum . not . empty . uncurry intersectRange)