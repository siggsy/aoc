module Days.Day16 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import Data.Map.Strict (Map, (!), (!?))
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import qualified Util.Util as U

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text hiding (takeWhile, take)
import Data.Void
import Data.Tree (Tree (Node), unfoldTree, foldTree, flatten)
import Data.Function
import Debug.Trace
import Data.Bits
{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

------------ PARSER ------------
inputParser :: Parser Input
inputParser = Map.fromList <$> valveParser `sepBy` endOfLine

valveParser :: Parser (String, (Int, [String]))
valveParser = do
  string "Valve "
  name <- many1 letter
  string " has flow rate="
  flow <- decimal
  choice [string "; tunnels lead to valves ", string "; tunnel leads to valve "]
  valves <- many1 letter `sepBy` string ", "
  pure (name, (flow, valves))

------------ TYPES ------------
type Input = ValveMap

type OutputA = (Int, Int, String, Set String)

type OutputB = Int

type ValveMap = Map String (Int, [String])
type DistanceMap = Map (String, String) Int

ordered :: Ord a => a -> a -> (a,a)
ordered a b =
  if a <= b
    then (a,b)
    else (b,a)

{-
>>> findLink "AA" "JJ" [("AA","AA"),("AA","BB"),("AA","CC"),("AA","DD"),("AA","II"),("II","II"),("II","JJ")]
Just "II"
-}
findLink :: String -> String -> [(String, String)] -> Maybe String
findLink a b links = let
  aLinked = map (\(x,y) -> if x == a then (a,y) else (a,x)) $ filter (\(x,y) -> x == a || y == a) links
  bLinked = map (\(x,y) -> if x == b then (b,y) else (b,x)) $ filter (\(x,y) -> x == b || y == b) links
  in case filter (\(a,x) -> any (\(b,x') -> x == x') bLinked) aLinked of
    [] -> Nothing
    (_,link):_ -> Just link

distances :: ValveMap -> DistanceMap
distances valveMap = Map.union directDistances proxyDistances
  where
    valveList = Map.toAscList valveMap

    identityDistances :: DistanceMap
    identityDistances = Map.fromList $ map identity valveList
    identity (pos, _) = ((pos,pos), 0)

    directDistances :: DistanceMap
    directDistances = Map.unions $ identityDistances : map (Map.fromList . direct) valveList
    direct (pos, (_, neighbours)) = map ((,1) . ordered pos) neighbours
    directPairs = Map.keysSet directDistances

    proxyDistances :: DistanceMap
    proxyDistances = proxy
      (Set.fromList
        [ (a,b)
          | (a,_) <- valveList
          , (b,_) <- valveList
          , a < b
          , (a,b) `notElem` directPairs ])
      directDistances

    proxy unresolved distanceMap =
      let
        resolved      = Map.keys distanceMap
        links         = mapMaybe (\pair@(a,b) -> (pair,) <$> findLink a b resolved) (Set.toList unresolved)
        newLinks      = Map.fromList $ map (\((a,b), link) -> (ordered a b, distanceMap ! ordered a link + distanceMap ! ordered b link)) links
        distanceMap'  = Map.union distanceMap newLinks
        unresolved'   = unresolved Set.\\ Map.keysSet newLinks
      in if null newLinks
        then distanceMap
        else proxy unresolved' distanceMap'

tunnelTree :: Int -> String -> ValveMap -> Tree (Int, Int, String, Set String)
tunnelTree minutes start valveMap = unfoldTree neighbours (minutes, 0, start, valvesWithoutAA)
  where
    valvesWithoutAA = Set.delete "AA" . Map.keysSet $ Map.filter (\(p,_) -> p /= 0) valveMap
    distanceMap = distances valveMap

    neighbours (minute, pressure, valve, vs) =
      let
        withoutValve = Set.delete valve vs

        state :: Int -> Int -> String -> Set String -> (Int, Int, String, Set String)
        state m pres v vm =
          let
            (p',_)  = valveMap ! v
            d       = distanceMap ! ordered valve v
          in (m-d-1, p' * (m-d-1) + pressure, v, Set.delete v vm)

        other = filter (\(m,_,_,_) -> m >= 0) $ map (\v -> state minute pressure v withoutValve) (Set.toList withoutValve)
      in ((minute, pressure, valve, withoutValve), other)

maximumBy' :: (a -> a -> Ordering) -> [a] -> a
maximumBy' _ []   =  error "List.maximumBy: empty list"
maximumBy' cmp xs =  foldl1' maxBy xs
  where
    maxBy x y = case cmp x y of
      GT -> x
      _  -> y

------------ PART A ------------
partA :: Input -> OutputA
partA = maximumBy' (compare `on` (\(_,p,_,_) -> p)) . flatten . tunnelTree 30 "AA"

------------ PART B ------------
partB :: Input -> OutputB
partB input =
  let
    valves = Map.keysSet $ Map.filter (\(p,_) -> p /= 0) input
    valvesLength = Set.size valves
    tree = flatten $ tunnelTree 26 "AA" input
    tree' = map (\(m,v,p,k) -> (valves Set.\\ k, (m,v,p,k))) tree
  in
    maximum
      [ v1+v2
        | (k1',(_,v1,_,k1)) <- tree'
        , (k2',(_,v2,_,k2)) <- tree'
        , valvesLength - Set.size k1 <= Set.size k2
        , Set.isSubsetOf k1' k2
        ]

combinations :: [a] -> [(a,a)]
combinations [] = []
combinations (a:as) = map (a,) as ++ combinations as