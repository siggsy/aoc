{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use newtype instead of data" #-}
module Days.Day11 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Sequence as Seq
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import qualified Util.Util as U

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text
import Data.Void
{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

------------ PARSER ------------
inputParser :: Parser Input
inputParser = do
    monkeys <- monkey `sepBy` many' endOfLine
    return . Map.fromList $ zip [0..] monkeys

monkey :: Parser Monkey
monkey = do
    string "Monkey "
    decimal
    char ':'

    items <- parseItems
    endOfLine
    operation <- parseOperation
    endOfLine

    Monkey items operation <$> parseTest'

parseItems :: Parser (Seq.Seq Integer)
parseItems = do
    many' space
    string "Starting items: "
    Seq.fromList <$> decimal `sepBy` string ", "

parseOperation :: Parser (Integer -> Integer)
parseOperation = do
    many' space
    string "Operation: new = old "
    operator <- anyChar
    let operation = case operator of
            '*' -> (*)
            '+' -> (+)
    space
    operand <- eitherP decimal (string "old")
    return $ case operand of
        Left val -> operation val
        Right _ -> (\op -> operation op op)

parseTest' :: Parser (Integer, Integer, Integer)
parseTest' = do
    many' space
    string "Test: divisible by "
    division <- decimal
    endOfLine

    many' space
    string "If true: throw to monkey "
    trueMonkey <- decimal
    endOfLine

    many' space
    string "If false: throw to monkey "
    falseMonkey <- decimal

    return (division, trueMonkey, falseMonkey)

------------ TYPES ------------
type Input = Monkeys

type OutputA = Integer

type OutputB = Integer

type Monkeys = Map.Map Integer Monkey

data Monkey = Monkey
    { items :: Seq.Seq Integer
    , operation :: Integer -> Integer
    , test :: (Integer, Integer, Integer)
    }

instance Show Monkey where
    show :: Monkey -> String
    show (Monkey items _ _) = "Monkey " ++ show items

turn :: Monkeys -> Integer -> (Monkeys, Integer)
turn monkeys monkeyNum =
    (foldl'
        (flip $ uncurry Map.adjust)
        monkeys
        monkeyUpdates, fromIntegral $ Seq.length (items monkey))
    where
        monkey :: Monkey
        monkey = monkeys Map.! monkeyNum

        monkeyOps :: Integer
        monkeyOps = product . Map.map ((\(a,_,_) -> a) . test) $ monkeys

        inspect :: Integer -> (Integer, Integer)
        inspect item = let
            item' = operation monkey item `mod` monkeyOps
            (op, true, false) = test monkey
            in if item' `mod` op == 0
                then (true, item')
                else (false, item')

        itemActions :: Seq.Seq (Integer, Integer)
        itemActions = fmap inspect (items monkey)

        throwTo :: Integer -> Integer -> (Monkey -> Monkey, Integer)
        throwTo toMonkey item = let
            monkey = monkeys Map.! toMonkey
            in (\monkey -> monkey { items = items monkey Seq.|> item }, toMonkey)

        monkeyUpdates :: Seq.Seq (Monkey -> Monkey, Integer)
        monkeyUpdates = (\monkey -> monkey { items = Seq.empty }, monkeyNum) Seq.<| fmap (uncurry throwTo) itemActions

round' :: Monkeys -> (Monkeys, Seq.Seq Integer)
round' monkeys 
    = foldl'
        turn'
        (monkeys, Seq.empty)
        (Map.keys monkeys)
    where
        turn' (monkeys, inspects) monkeyNum = let
            (monkeys', inspections) = turn monkeys monkeyNum
            in (monkeys', inspects Seq.|> inspections)

run :: Monkeys -> Integer -> (Monkeys, Seq.Seq Integer)
run monkeys rounds 
    = foldl' 
        round''
        (monkeys, Seq.replicate (length $ Map.keys monkeys) 0) 
        [1..rounds]
    where
        round'' (monkeys, inspects) round = let
            (monkeys', inspects') = round' monkeys
            in (monkeys', Seq.zipWith (+) inspects inspects')

------------ PART A ------------
partA :: Input -> OutputA
partA monkeys 
    = product 
    . Seq.take 2 
    . Seq.reverse 
    . Seq.sort 
    . snd 
    $ run monkeys 12

------------ PART B ------------
partB :: Input -> OutputB
partB monkeys 
    = product
    . Seq.take 2
    . Seq.reverse
    . Seq.sort
    . snd
    $ run monkeys 10000
