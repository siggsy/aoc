module Days.Day07 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import qualified Data.List.Split as Split
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import qualified Util.Util as U

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text
import Data.Void
{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

data Command =
    CD String |
    LS [File]
    deriving Show

data File =
    File Int String |
    Dir String
    deriving Show

data FS =
    Directory String [FS] |
    FileEntry Int String
    deriving Show

fileSize (File size _) = size
fileSize (Dir _) = 0

name (File _ n) = n
name (Dir n) = n

makeFs :: [Command] -> Map.Map [String] [File]
makeFs commands = let

    makeMap dirs fs [] = fs
    makeMap dirs fs (CD "..":cs) = makeMap (tail dirs) fs cs
    makeMap dirs fs (CD name:cs) = makeMap (name:dirs) fs cs
    makeMap dirs fs (LS files:cs) = makeMap dirs (Map.insert dirs files fs) cs

    in makeMap [] Map.empty commands

------------ PARSER ------------
inputParser :: Parser Input
inputParser = choice [cd, ls] `sepBy` endOfLine

dir :: Parser File
dir = do
    string "dir "
    name <- many1' (notChar '\n')
    return $ Dir name

file :: Parser File
file = do
    size <- decimal
    space
    name <- many1' (notChar '\n')
    return (File size name)

cd :: Parser Command
cd = do
    string "$ cd "
    arg <- many1' (notChar '\n')
    return $ CD arg

ls :: Parser Command
ls = do
    string "$ ls"
    endOfLine
    files <- choice [dir, file] `sepBy` endOfLine
    return $ LS files

------------ TYPES ------------
type Input = [Command]

type OutputA = Int

type OutputB = Int

------------ PART A ------------
partA :: Input -> OutputA
partA commands = let
    fs = makeFs commands
    sumsBelow100000 = Map.filter (<= 100000) (dirSizeMap ["/"] fs)
    in sum . map snd . Map.assocs $ sumsBelow100000

dirSizeMap :: [String] -> Map.Map [String] [File] -> Map.Map [String] Int
dirSizeMap path fs = let
    contents = fs Map.! path
    (files, dirs) = partition (\case File _ _ -> True; Dir _ -> False) contents

    fileSum = sum . map fileSize $ files
    subDirs = map (\dir -> dirSizeMap (name dir:path) fs) dirs

    dirSums = foldr1 (Map.unionWith (+)) subDirs
    dirSum = sum . map snd . Map.assocs . Map.filterWithKey (\(p:ps) _-> ps == path) $ dirSums
    in if not . null $ dirs
        then Map.insert path (fileSum + dirSum) dirSums
        else Map.insert path fileSum Map.empty

------------ PART B ------------
partB :: Input -> OutputB
partB commands = let
    fs = makeFs commands
    dirSizes = dirSizeMap ["/"] fs
    requiredSpace = 30000000
    totalSpace = 70000000
    freeSpace = totalSpace - dirSizes Map.! ["/"]
    in head 
        . dropWhile (\s -> freeSpace + s < requiredSpace) 
        . sort 
        . map snd 
        . Map.assocs $ dirSizes
