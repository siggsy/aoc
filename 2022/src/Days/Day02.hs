module Days.Day02 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import qualified Util.Util as U

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text
import Data.Void
{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

data RPS = 
    Rock | 
    Paper | 
    Scissors | 
    Draw deriving Eq

rps :: RPS -> RPS -> RPS
rps Rock Paper = Paper
rps Rock Scissors = Rock
rps Paper Rock = Paper
rps Paper Scissors = Scissors
rps Scissors Rock = Rock
rps Scissors Paper = Scissors
rps _ _ = Draw

value :: RPS -> Int
value Rock = 1
value Paper = 2
value Scissors = 3

score :: RPS -> RPS -> Int
score opponent me = let 
    winner = rps opponent me
    score 
        | winner == me = 6
        | winner == opponent = 0
        | winner == Draw = 3
    in score + value me

fromABC :: Char -> RPS
fromABC 'A' = Rock
fromABC 'B' = Paper
fromABC 'C' = Scissors

fromXYZ :: Char -> RPS
fromXYZ 'X' = Rock
fromXYZ 'Y' = Paper
fromXYZ 'Z' = Scissors

fromXYZ' :: Char -> RPS -> RPS
fromXYZ' 'X' opponent = case opponent of
    Rock -> Scissors
    Paper -> Rock
    Scissors -> Paper
fromXYZ' 'Y' opponent = opponent
fromXYZ' 'Z' opponent = case opponent of
    Rock -> Paper
    Paper -> Scissors
    Scissors -> Rock

-- PARSER ------------
inputParser :: Parser Input
inputParser = many1 $ do
    c1 <- anyChar
    space
    c2 <- anyChar 
    many' space
    return (c1, c2)

-- TYPES ------------
type Input = [(Char, Char)]

type OutputA = Int

type OutputB = Int

-- PART A ------------
partA :: Input -> OutputA
partA = sum . map play

play :: (Char, Char) -> Int
play (abc, xyz) = let
    opponent    = fromABC abc
    me          = fromXYZ xyz
    in score opponent me

-- PART B ------------
partB :: Input -> OutputB
partB = sum . map play'

play' :: (Char, Char) -> Int
play' (abc, xyz) = let
    opponent    = fromABC abc
    me          = fromXYZ' xyz opponent
    in score opponent me
