module Days.Day17 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import qualified Util.Util as U

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text hiding (take)
import Data.Void
import Data.Bifunctor
import Debug.Trace

{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

------------ PARSER ------------
inputParser :: Parser Input
inputParser = many1 $ choice [char '<' >> pure L, char '>' >> pure R]

------------ TYPES ------------
type Input = [Move]

type OutputA = Int
-- type OutputA = [U.Grid Material]
-- type OutputA = (Int, Shape)

type OutputB = Int

data Move = L | R deriving Show
data Material = Rock | Air deriving (Eq, Ord)
type Shape = Set (Int, Int)

instance Show Material where
  show :: Material -> String
  show Rock = "#"
  show Air = "."

shape :: [[Char]] -> Set (Int, Int)
shape
  = Map.keysSet
  . invertY
  . Map.filter (== Rock)
  . U.mapFromNestedLists
  . map
    ( map (\case
      '#' -> Rock
      '.' -> Air) )

showShape :: Shape -> U.Grid Material
showShape = U.Grid . invertY . Map.fromList . map (,Rock) . Set.toList

invertY :: Map (Int, Int) a -> Map (Int, Int) a
invertY m = Map.mapKeys (first (bottom -)) m
  where
    (top,bottom,left,right) = U.mapBoundingBox m

shapes :: [Shape]
shapes = map shape
  [ [ "####" ]

  , [ ".#."
    , "###"
    , ".#."
    ]

  , [ "..#"
    , "..#"
    , "###"
    ]

  , [ "#"
    , "#"
    , "#"
    , "#"
    ]

  , [ "##"
    , "##"
    ]
  ]

drop' :: Shape -> Shape
drop' = Set.mapMonotonic (\(y,x) -> (y-1,x))

move :: Shape -> Move -> Shape
move s m =
  Set.mapMonotonic
    (\(y,x) -> case m of
      L -> (y,x-1)
      R -> (y,x+1))
    s

collided :: Shape -> ((Int, Int, Int), Shape) -> Bool
collided shape ((left,right,bottom),frozen) =
  let
    (_bottom,_,_left,_right) = U.setBoundingBox shape
  in
    bottom == _bottom
    || left == _left
    || right == _right
    || not (Set.null (Set.intersection shape frozen))

start :: Shape -> Shape -> Shape
start s frozen =
  let
    (fb, ft, fl, fr) = U.setBoundingBox frozen
  in if Set.null frozen
    then Set.mapMonotonic (\(y,x) -> (y+(ft+3), x+2)) s
    else Set.mapMonotonic (\(y,x) -> (y+(ft+4), x+2)) s

mapHead :: (a -> a) -> [a] -> [a]
mapHead f [] = []
mapHead f (a:as) = f a : as

maybeEmpty ::  a -> Set a -> Set a
maybeEmpty a set
 | Set.null set = Set.singleton a
 | otherwise = set

eq :: ((Int,Shape), (Int,Move), Shape) -> ((Int,Shape), (Int,Move), Shape) -> Bool
eq ((asi,_), (ami,_), aMap) ((bsi,_), (bmi,_), bMap) =
  asi == bsi && ami == bmi && aMap ~=~ bMap

(~=~) :: Shape -> Shape -> Bool
(~=~) a b =
  let
    (aBottom, _, _, _) = U.setBoundingBox a
    (bBottom, _, _, _) = U.setBoundingBox b
    aNormalized = Set.mapMonotonic (\(y,x) -> (y-aBottom,x)) a
    bNormalized = Set.mapMonotonic (\(y,x) -> (y-bBottom,x)) b
  in aNormalized == bNormalized

{-

>>> Set.difference (inflate 3 (Set.fromList [(3,0), (3,1), (3,2)])) (Set.fromList [(2,0)])
fromList [(2,1),(2,2),(2,3),(3,3)]
-}

inflate :: Int -> Shape -> Shape
-- inflate top shape | trace ("inflate " ++ show shape ++ "\n") False = undefined
inflate top shape =
  Set.unions
      [ Set.filter (\(y,x) -> y <= top && y >= 0 && x >= 0 && x <= 6)
      . Set.mapMonotonic (\(y,x) -> (y+dy,x+dx))
          $ shape
        | (dy, dx) <- [ (0,-1), (0,1), (-1,-1), (-1,0), (-1,1), (1,-1), (1,0), (1,1) ]
        ]

outline :: Shape -> Shape
outline s =
  let
    (bottom,top,left,right) = U.setBoundingBox s
    startingRow = Set.fromList $ map (top,) [0..6]

    freezeEdge :: Shape -> Shape
    freezeEdge = Set.intersection s

    go :: Shape -> Shape -> Shape -> Shape
    -- go inflating edge searched | trace (show inflating ++ "\n" ++ show edge ++ "\n") False = undefined
    go inflating edge searched =
      let
        inflated = inflate top inflating Set.\\ searched
        edge' = freezeEdge inflated
      in
        if Set.null inflating
          then edge
          else go (Set.difference inflated edge') (Set.union edge edge') (Set.union searched inflated)

  in go startingRow (freezeEdge startingRow) startingRow


simulate :: Int -> [Move] -> [((Int,Shape), (Int,Move), Shape)]
simulate steps moves =
  wind
    ( mapHead ((,) <$> fst <*> (`start` Set.empty) . snd)
    . take steps
    . cycle
      $ zip [0..] shapes )
    (cycle (zip [0..] moves))
    Set.empty
  where
    bounds = (-1,7,-1)

    fall :: [(Int, Shape)] -> [(Int, Move)] -> Shape -> [((Int,Shape), (Int,Move), Shape)]
    fall []         _     frozen = []
    fall ((si,s):ss) moves@((mi,m):ms) frozen =
      let
        dropped = drop' s
        falling = Set.union frozen s
        frozen' = outline falling
        new     = mapHead ((,) <$> fst <*> (`start` frozen') . snd)
      in
        if dropped `collided` (bounds, frozen)
          then ((si,s), (mi,m), frozen') : wind (new ss)          moves frozen'
          else                             wind ((si,dropped):ss) moves frozen

    wind :: [(Int, Shape)] -> [(Int, Move)] -> Shape -> [((Int,Shape), (Int,Move), Shape)]
    wind []           _          frozen = []
    wind ((si,s):ss) ((mi,m):ms) frozen =
      let moved = move s m
      in if moved `collided` (bounds, frozen)
        then fall ((si,s):ss)     ms frozen
        else fall ((si,moved):ss) ms frozen

height :: Shape -> Int
height = (\(_,t,_,_) -> t) . U.setBoundingBox

------------ PART A ------------
partA :: Input -> OutputA
partA input = succ . height . (\(_,_,s) -> s) .  last $ simulate 2022 input

------------ PART B ------------
partB :: Input -> OutputB
partB input =
  let
    simulated         = simulate 1000000000000 input
    heightDeltas      = zipWith (\((asi,_),(ami,_),a) ((bsi,_),(bmi,_),b) -> (asi, bsi, ami, bmi, height b - height a)) simulated (drop 1 simulated)
    (notCycle,cycle)  = findCycle heightDeltas
    heightDeltaSum    = sum . map (\(_,_,_,_,d) -> d)
    notCycleHeight    = heightDeltaSum notCycle
    cycleHeight       = heightDeltaSum cycle
    (cycles,rest)     = (1000000000000 - length notCycle) `divMod` length cycle
  in notCycleHeight + cycleHeight * cycles + heightDeltaSum (take rest cycle)

findCycle :: Eq a => [a] -> ([a],[a])
findCycle xxs = fCycle xxs xxs
  where
    fCycle (x:xs) (_:y:ys)
      | x == y              = fStart xxs xs
      | otherwise           = fCycle xs ys
    fCycle _      _         = (xxs,[])
    fStart (x:xs) (y:ys)
      | x == y              = ([], x:fLength x xs)
      | otherwise           = let (as,bs) = fStart xs ys in (x:as,bs)
    fLength x (y:ys)
      | x == y              = []
      | otherwise           = y:fLength x ys