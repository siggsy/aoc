module Days.Day01 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import qualified Data.List as L
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import qualified Util.Util as U

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text
import Data.Void
import Data.Ord
{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

-- PARSER ------------
inputParser :: Parser Input
inputParser = (decimal `sepBy` char '\n') `sepBy` char '\n'

-- TYPES ------------
type Input = [[Int]]

type OutputA = Int

type OutputB = Int

-- PART A ------------
partA :: Input -> OutputA
partA = maximum . map sum

-- PART B ------------
partB :: Input -> OutputB
partB = sum . L.take 3 . sortOn Down . map sum