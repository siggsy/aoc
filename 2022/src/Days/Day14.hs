module Days.Day14 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import qualified Util.Util as U
import Util.Parsers

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text hiding (takeWhile)
import Data.Void
{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

------------ PARSER ------------
inputParser :: Parser Input
inputParser = decimal `around` char ',' `sepBy` string " -> " `sepBy` endOfLine

------------ TYPES ------------
type Input = [[Pos]]

type OutputA = Int

type OutputB = Int

data Material
  = Air
  | Sand
  | Rock
  deriving Eq

instance Show Material where
  show :: Material -> String
  show Air = "."
  show Sand = "o"
  show Rock = "#"

type Pos = (Int, Int)
type Sim = Map Pos Material

newtype Grid = Grid Sim
instance Show Grid where
  show :: Grid -> String
  show (Grid sim) = let
    (left, right, top, bottom) = U.mapBoundingBox sim
    in unlines . U.chunksOf (right-left+1) . concat $
      [ show material
      | y <- [top..bottom]
      , x <- [left..right]
      , let material = fromMaybe Air $ sim Map.!? (x,y)
      ]

line :: [Pos] -> Sim
line [] = Map.empty
line [p] = Map.empty
line ((x,y):(x',y'):ps)
  = Map.unionWith const (line ((x',y'):ps))
  . Map.fromList
  $ zipWith
      (curry (, Rock))
      [x,x+signum (x'-x)..x']
      [y,y+signum (y'-y)..y']

createMap :: [[Pos]] -> Sim
createMap = Map.unionsWith const . map line

dropSand :: Sim -> [Pos]
dropSand sim = 
  if isNothing (sim Map.!? (500,0))
    then drop (500,0)
    else [(500,0)]
  where
    drop pos@(x,y) = let
      left  = sim Map.!? (x-1,y)
      right = sim Map.!? (x+1,y)
      curr  = sim Map.!? (x,y)
      in case (left, right, curr) of
        (_,       _,        Nothing)  -> pos : drop (x,y+1)
        (Nothing, _,        Just _)   -> (x-1,y) : drop (x-1,y+1)
        (_,       Nothing,  Just _)   -> (x+1,y) : drop (x+1,y+1)
        _                             -> []

finalPosition :: Int -> [Pos] -> Pos
finalPosition _ [p] = p
finalPosition 0 (p:ps) = p
finalPosition bottom (p:ps) = finalPosition (bottom-1) ps

fill :: Int -> (Pos -> Bool) -> Sim -> Sim
fill bottom predicate sim = let
  finalPos  = finalPosition bottom $ dropSand sim
  sim'      = Map.insert finalPos Sand sim
  in if predicate finalPos
    then fill bottom predicate sim'
    else sim'

sandCount :: Sim -> Int
sandCount 
  = length
  . map snd
  . Map.toList
  . Map.filter (== Sand)

------------ PART A ------------
partA :: Input -> OutputA
partA input = let
  map             = createMap input
  (_,_,_,bottom)  = U.mapBoundingBox map
  in pred
    . sandCount
    . fill bottom (\(x,y) -> y < bottom)
    $ map

------------ PART B ------------
partB :: Input -> OutputB
partB input = let
  map             = createMap input
  (_,_,_,bottom)  = U.mapBoundingBox map
  in sandCount 
    . fill (bottom+1) (/= (500,0))
    $ map

