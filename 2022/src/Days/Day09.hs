module Days.Day09 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import qualified Util.Util as U

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text hiding (D)
import Data.Void
import Control.Lens
{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

type Move = (Int, Int)

move :: [(Int, Int)] -> Move -> [(Int, Int)]
move ((x,y):tail) (x', y') = let
    in follow ((x+x', y+y'):tail)

follow :: [(Int, Int)] -> [(Int, Int)]
follow [] = []
follow [h] = [h]
follow (h@(hx,hy) : t@(tx,ty) : rest) = let
    tx' = signum $ hx - tx
    ty' = signum $ hy - ty
    t' = (tx+tx', ty+ty')
    in if distance (h, t) > 1
        then h : follow (t':rest)
        else h : t : rest

distance :: ((Int, Int), (Int, Int)) -> Int
distance ((hx, hy), (tx, ty)) =
    max (abs (hx-tx)) (abs (hy-ty))

explode :: [Move] -> [Move]
explode [] = []
explode ((0,0):ms) = explode ms
explode ((x,y):ms) = let
    xUnit = signum x
    yUnit = signum y
    in (xUnit, yUnit) : explode ((x-xUnit, y-yUnit):ms)

------------ PARSER ------------
inputParser :: Parser Input
inputParser = move' `sepBy` endOfLine

move' :: Parser Move
move' = do
    direction <- letter
    space
    count <- decimal
    return $ case direction of
        'R' -> (count, 0)
        'L' -> (-count, 0)
        'D' -> (0, -count)
        'U' -> (0, count)

------------ TYPES ------------
type Input = [Move]

type OutputA = Int

type OutputB = Int

------------ PART A ------------
partA :: Input -> OutputA
partA = length
    . nub
    . map last
    . scanl move [(0,0),(0,0)]
    . explode

------------ PART B ------------
partB :: Input -> OutputB
partB = length
    . nub
    . map last
    . scanl move (replicate 10 (0,0))
    . explode
