module Days.Day15 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import qualified Util.Util as U

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text
import Data.Void
import Data.Function
{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

------------ PARSER ------------
inputParser :: Parser Input
inputParser = do
  sbs <- parseSensor `sepBy` endOfLine
  return $ zipWith (\(a,b) c -> (a, b, manhattan a b)) sbs [0..]

parseSensor :: Parser (Pos, Pos)
parseSensor = do
  string "Sensor at "
  sensorPosition <- parsePos
  string ": closest beacon is at "
  beaconPosition <- parsePos
  return (sensorPosition, beaconPosition)

parsePos :: Parser Pos
parsePos = do
  string "x="
  x <- signed decimal
  string ", y="
  y <- signed decimal
  return (x,y)

------------ TYPES ------------
type Input = [(Pos, Pos, Int)]

type OutputA = Int

type OutputB = Int

type Pos = (Int, Int)
data Tile
  = Sensor
  | Beacon
  | Scanned
  | Unknown
  deriving (Ord, Eq)

instance Show Tile where
  show :: Tile -> String
  show Sensor = "S"
  show Beacon = "B"
  show Scanned = "#"
  show Unknown = "."

manhattan :: Pos -> Pos -> Int
manhattan (x,y) (a,b) = abs (x-a) + abs (y-b)

{-
>>> circleAround ((0,0), (0,0), 1)
[(0,-2),(-1,-1),(1,-1),(-2,0),(2,0),(-1,1),(1,1),(0,2)]
-}
circleAround :: (Pos, Pos, Int) -> [Pos]
circleAround ((x,y), beacon, distance) =
  [ (x', y')
    | let ring = distance + 1
    , y' <- [y - ring .. y + ring]

    , let diff = ring - abs (y-y')
    , x' <- if diff /= 0
        then [x - diff, x + diff]
        else [x] ]

sample :: Pos -> [(Pos, Pos, Int)] -> Tile
sample position sbs =
  if any (position `inRange`) sbs
    then Scanned
    else Unknown
  where
    inRange pos (sensorPos, beaconPos, distance) =
      sensorPos /= pos &&
      beaconPos /= pos &&
      manhattan pos sensorPos <= distance

------------ PART A ------------
partA :: Input -> OutputA
partA input = 
  let
    maxLeft   = minimum . map (\((x,y), (x',y'), distance) -> min x' (x-distance)) $ input
    maxRight  = maximum . map (\((x,y), (x',y'), distance) -> max x' (x+distance)) $ input
  in 
    length . filter (== Scanned) $
    [ sample (x, 2000000) input
      | x <- [maxLeft .. maxRight] ]

------------ PART B ------------
partB :: Input -> OutputB
partB input =
  let 
    toSample = concatMap circleAround input
    tuningFrequency x y = x * 4000000 + y 
  in
    uncurry tuningFrequency . head $
    [ pos
      | pos@(x,y) <- toSample
      , x >= 0 && x <= 4000000
      , y >= 0 && y <= 4000000
      , let sampled = sample pos input 
      , sampled == Unknown ]
