module Days.Day18 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import qualified Util.Util as U

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text hiding (takeWhile)
import Data.Void
import qualified Debug.Trace as U
{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

------------ PARSER ------------
inputParser :: Parser Input
inputParser = triple `sepBy` endOfLine

triple :: Parser (Int, Int, Int)
triple = do
  t1 <- decimal
  char ','
  t2 <- decimal
  char ','
  t3 <- decimal
  return (t1,t2,t3)

------------ TYPES ------------
type Input = [(Int, Int, Int)]

type OutputA = Int

type OutputB = Int

type Shape = Set (Int, Int, Int)

neighbours :: [(Int, Int, Int)]
neighbours =
  [ (dx,dy,dz)
    | let op = [(+),(-), const]
    , dx <- map (\o -> o 0 1) op
    , dy <- map (\o -> o 0 1) op
    , dz <- map (\o -> o 0 1) op
    , abs dx + abs dy + abs dz == 1
    ]

inflate :: (Int, Int, Int, Int, Int, Int) -> Shape -> Shape
inflate (left,right,top,bottom,up,down) shape =
  Set.unions
      [ Set.filter 
        (\(x,y,z) -> 
          y >= top && 
          y <= bottom && 
          x >= left && 
          x <= right && 
          z >= up && 
          z <= down)
      . Set.mapMonotonic (\(x,y,z) -> (x+dx,y+dy,z+dz))
        $ shape
        | (dx,dy,dz) <- neighbours
        ]

water :: Shape -> Shape
water shape =
  let
    cube@(left,right,top,bottom,up,down) = boundingCube shape
    cubePlusOne = (left-1,right+1,top-1,bottom+1,up-1,down+1)
    startingRow = Set.fromList [(left-1,top-1,up-1)]

    go :: Shape -> Shape -> Shape
    go inflating searched =
      let
        inflated = (inflate cubePlusOne inflating Set.\\ searched) Set.\\ shape
      in
        if Set.null inflating
          then searched
          else go inflated (Set.union searched inflated)

  in go startingRow startingRow

boundingCube :: Set (Int, Int, Int) -> (Int, Int, Int, Int, Int, Int)
boundingCube s =
  (,,,,,)
    (minimum . fmap (\(i,_,_) -> i) $ l)
    (maximum . fmap (\(i,_,_) -> i) $ l)
    (minimum . fmap (\(_,i,_) -> i) $ l)
    (maximum . fmap (\(_,i,_) -> i) $ l)
    (minimum . fmap (\(_,_,i) -> i) $ l)
    (maximum . fmap (\(_,_,i) -> i) $ l)
    where
      l = Set.toList s

surfaceArea :: Shape -> Int
surfaceArea shape = 
  sum . map 
    ( (6-) 
    . Set.size 
    . Set.intersection shape 
    . (\(x,y,z) -> Set.fromList [ (x+dx,y+dy,z+dz) | (dx,dy,dz) <- neighbours ])
    )
    $ Set.toList shape

------------ PART A ------------
partA :: Input -> OutputA
partA = surfaceArea . Set.fromList

------------ PART B ------------
partB :: Input -> OutputB
partB input =
  let
    lava = Set.fromList input
    (left,right,top,bottom,up,down) = boundingCube lava
    waterFilled = water lava
    airPockets =
      Set.fromList
        [ (x,y,z)
          | x <- [ left .. right  ]
          , y <- [  top .. bottom ]
          , z <- [   up .. down   ]
          ]
      Set.\\ Set.union waterFilled lava

    filledAirPockets = Set.union lava airPockets
  in surfaceArea filledAirPockets