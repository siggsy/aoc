module Days.Day12 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import Data.Map.Strict (Map, (!?))
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import qualified Util.Util as U

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text
import Data.Void
import Data.Char
import Data.Function
import Control.Monad
{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

------------ PARSER ------------
inputParser :: Parser Input
inputParser = U.mapFromNestedLists . map (map height) <$> many1 letter `sepBy` endOfLine

------------ TYPES ------------
type Input = Grid

type OutputA = Int

type OutputB = Int

type Coord = (Int, Int)
data Height = Start | Height Char | End deriving (Eq, Ord, Show)
type Grid = Map Coord Height

instance Enum Height where
    toEnum :: Int -> Height
    toEnum  0 = Start
    toEnum 27 = End
    toEnum  n = Height $ toEnum $ n + 96

    fromEnum :: Height -> Int
    fromEnum  Start     = 0
    fromEnum  End       = 27
    fromEnum (Height c) = subtract 96 $ fromEnum c

height :: Char -> Height
height 'S' = Start
height 'E' = End
height  c  = Height c

neighbours :: Grid -> Grid
neighbours g = Map.unionsWith max
    [ Map.mapKeysMonotonic (\(x, y) -> (x `op` dx, y `op` dy)) g
    | (dx, dy) <- [(1, 0), (0, 1)], op <- [(-), (+)]
    ]

constrain :: Grid -> Grid -> (Grid, Grid)
constrain g0 g =
    let g' = Map.mergeWithKey
            (\_ h0 h -> guard (succ h >= h0) *> Just h0)
            (const Map.empty) (const Map.empty) g0 g
        g0' = g0 Map.\\ g'
    in  (g0', g')

shortestPath :: (Height -> Bool) -> Grid -> Int
shortestPath test g0 = go g0 $ Map.filter test g0
    where
        go g0 g
            | End `elem` g = 0
            | otherwise    = succ $ uncurry go $ constrain g0 $ neighbours g

------------ PART A ------------
partA :: Input -> OutputA
partA = shortestPath (== Start)

------------ PART B ------------
partB :: Input -> OutputB
partB = shortestPath (== Height 'a')
