module Days.Day10 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import qualified Util.Util as U

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text
import Data.Void
import Control.Lens
{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

------------ PARSER ------------
inputParser :: Parser Input
inputParser = parseCommand `sepBy` endOfLine

parseCommand :: Parser Instruction
parseCommand = choice [addx, noop]
    where
        addx = string "addx " >> ADDX <$> signed decimal
        noop = string "noop" >> return NOOP

------------ TYPES ------------
type Input = [Instruction]
type OutputA = Int
type OutputB = CRT

data Instruction
    = NOOP
    | ADDX Int
    deriving Show

runCommand :: Int -> Instruction -> (Int, Int)
runCommand x NOOP = (x, 1)
runCommand x (ADDX val) = (x+val, 2)

run :: Int -> [Instruction] -> [Int]
run state [] = []
run state (i:is) = 
    let (state', cycles) = runCommand state i
    in replicate cycles state ++ run state' is

------------ PART A ------------
partA :: Input -> OutputA
partA 
    = sum
    . zipWith 
        (\i a -> if i `elem` [20,60,100,140,180,220]
            then a * i
            else 0)
        [1..]
    . run 1

------------ PART B ------------
partB :: Input -> OutputB
partB 
    = CRT 
    . zipWith draw [0..]
    . run 1

newtype CRT = CRT String
instance Show CRT where
    show :: CRT -> String
    show (CRT output) 
        = unlines 
        . U.chunksOf 40 
            $ output

draw :: Int -> Int -> Char
draw clock spritePos =
    if clock `mod` 40 `elem` [spritePos-1 .. spritePos+1]
        then '#'
        else '.'