module Days.Day06 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import qualified Util.Util as U

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text
import Data.Void
{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

------------ PARSER ------------
inputParser :: Parser Input
inputParser = many1' letter

------------ TYPES ------------
type Input = String

type OutputA = Int

type OutputB = Int

------------ PART A ------------
partA :: Input -> OutputA
partA = nDistinct 4 

nDistinct :: Ord a => Int -> [a] -> Int
nDistinct n = (+n) 
    . fromJust
    . elemIndex n 
    . map (length . nub) 
    . windows n

windows :: Int -> [a] -> [[a]]
windows n = filter ((== n) . length) 
    . map (Data.List.take n) 
    . tails

------------ PART B ------------
partB :: Input -> OutputB
partB = nDistinct 14