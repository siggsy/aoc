module Days.Day03 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import qualified Util.Util as U

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text
import Data.Void
import Data.Char
{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

value :: Char -> Int
value char = if char >= 'a'
    then ord char - ord 'a' + 1
    else ord char - ord 'A' + 27

rucksackPriority :: [Char] -> Int
rucksackPriority rucksack = let
    len = length rucksack
    (first, second) = splitAt (len `div` 2) rucksack
    inBoth = Set.intersection (Set.fromList first) (Set.fromList second)
    in sum $ Set.map value inBoth

groupCommon :: [String] -> Char
groupCommon = fromMaybe 'a' 
    . Set.lookupMax 
    . foldr1 Set.intersection 
    . map Set.fromList

-- PARSER ------------
inputParser :: Parser Input
inputParser = many1 letter `sepBy` char '\n'

-- TYPES ------------
type Input = [String]

type OutputA = Int

type OutputB = Int

-- PART A ------------
partA :: Input -> OutputA
partA = sum . map rucksackPriority

-- PART B ------------
partB :: Input -> OutputB
partB = sum . map (value . groupCommon) . U.chunksOf 3
