module Days.Day13 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import Util.Util as U
import Util.Parsers
import Util.Pair

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text
import Data.Void
import Data.Type.Ord (Compare)
{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

------------ PARSER ------------
inputParser :: Parser Input
inputParser = (nestedList `around` endOfLine) `sepBy` count 2 endOfLine

nestedList :: Parser NestedList
nestedList = 
  eitherP 
    (char '[' *> nestedList `sepBy` char ',' <* char ']')
    decimal 
  >>= \case
  Left list -> return $ List list
  Right int -> return $ Node int

------------ TYPES ------------
type Input = [(NestedList, NestedList)]

type OutputA = Int

type OutputB = Int

-- Data type for nested lists (eg.: [1,2,[3],4,[5,[6,7]]])
data NestedList
    = List [NestedList]
    | Node Int
    deriving (Show, Eq)

instance Ord NestedList where
  compare :: NestedList -> NestedList -> Ordering
  compare (Node a) (Node b) = compare a b

  compare (List [])     (List [])     = EQ
  compare (List [])     (List (b:_))  = LT
  compare (List (a:_))  (List [])     = GT
  compare (List (a:as)) (List (b:bs)) = compare a b <> compare (List as) (List bs)

  compare (List a) (Node b) = compare (List a) (List [Node b])
  compare (Node a) (List b) = compare (List [Node a]) (List b)

------------ PART A ------------
partA :: Input -> OutputA
partA 
  = sum 
  . zipWith
    (\i ord ->
      if ord == LT
        then i
        else 0)
    [1..] 
  . map (uncurry compare)

------------ PART B ------------
partB :: Input -> OutputB
partB = let
  __2__ = List[List[Node 2]]
  __6__ = List[List[Node 6]]
  in product
    . zipWith (\i l ->
        if l `elem` [__2__, __6__]
          then i
          else 1)
        [1..]
    . sort
    . (__2__:)
    . (__6__:)
    . uncurry (<>)
    . unzip
