module Days.Day05 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import qualified Util.Util as U

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text
import Data.Void
{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

------------ PARSER ------------
inputParser :: Parser Input
inputParser = do
    s <- state
    many' (char '\n')
    commands <- command `sepBy` char '\n'
    return (s, commands)

command :: Parser (Int, Int, Int)
command = do
    string "move "
    num <- decimal
    string " from "
    from <- decimal
    string " to "
    to <- decimal
    return (num, from, to)

state :: Parser [[Char]]
state = do
    cL <- many1 crateLine
    pileNumber `sepBy` char ' '
    return $ foldr mergeCrates [[]] cL

pileNumber :: Parser Int
pileNumber = do
    char ' '
    d <- decimal
    char ' '
    return d

crateLine :: Parser [Maybe Char]
crateLine = do
    line <- crate `sepBy` char ' '
    char '\n'
    return line

crate :: Parser (Maybe Char)
crate = do
    c <- peekChar
    case c of
        Just ' ' -> do
            count 3 (char ' ')
            return Nothing
        Just '[' -> do
            char '['
            l <- letter
            char ']'
            return $ Just l

        _ -> return Nothing

mergeCrates :: [Maybe Char] -> [[Char]] -> [[Char]]
mergeCrates (a:as) (as':ass') = case a of
    Just crate -> (crate : as') : mergeCrates as ass'
    Nothing -> as' : mergeCrates as ass'

mergeCrates [] ass = ass
mergeCrates (a:as) [] = case a of
    Just crate -> [crate] : mergeCrates as []
    Nothing -> [] : mergeCrates as []

------------ TYPES ------------
type Input = ([[Char]], [(Int, Int, Int)])

type OutputA = String

type OutputB = String

------------ PART A ------------
partA :: Input -> OutputA
partA (state, commands) = map head $ run9000 commands state

run9000 :: [(Int, Int, Int)]  -> [[Char]] -> [[Char]]
run9000 cs state = foldl (flip on9000) state cs

on9000 :: (Int, Int, Int) -> [[Char]] -> [[Char]]
on9000 (0, from, to) state = state
on9000 (num, from, to) state = (num-1, from, to) `on9000` move from to state

move :: Int -> Int -> [[Char]] -> [[Char]]
move = moveMany 1

------------ PART B ------------
partB :: Input -> OutputB
partB (state, commands) = map head $ run9001 commands state

run9001 :: [(Int, Int, Int)] -> [[Char]] -> [[Char]]
run9001 cs state = foldl (flip on9001) state cs 

on9001 :: (Int, Int, Int) -> [[Char]] -> [[Char]]
on9001 (num, from, to) = moveMany num from to

moveMany :: Int -> Int -> Int -> [[Char]] -> [[Char]]
moveMany num from to state = let
    toMove = Data.List.take num $ state !! (from-1)

    topMap :: Int -> [Char] -> [Char]
    topMap i x
        | i == to = toMove ++ x
        | i == from = drop num x
        | otherwise = x

    in zipWith topMap [1..] state
