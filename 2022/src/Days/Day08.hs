module Days.Day08 (runDay) where

{- ORMOLU_DISABLE -}
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector (Vector)
import qualified Data.Vector as Vec
import qualified Util.Util as U

import qualified Program.RunDay as R (runDay, Day)
import Data.Attoparsec.Text
import Data.Void
import Data.Char
{- ORMOLU_ENABLE -}

runDay :: R.Day
runDay = R.runDay inputParser partA partB

------------ PARSER ------------
inputParser :: Parser Input
inputParser = (map toInt <$> many1 digit) `sepBy` endOfLine
    where toInt char = ord char - ord '0'

------------ TYPES ------------
type Input = [[Int]]

type OutputA = Int

type OutputB = Int

------------ PART A ------------
partA :: Input -> OutputA
partA treeMap = let
    height = length treeMap
    width = length . head $ treeMap
    exterior = height * 2 + (width-2) * 2
    interiorVisible
        = sum
        . map fromEnum $
            [ isVisible (i, j) treeMap
            | i <- [1..height - 2]
            , j <- [1..width - 2]
            ]
    in exterior + interiorVisible

isVisible :: (Int, Int) -> [[Int]] -> Bool
isVisible (i,j) treeMap = let
    (front, t_ij:back)  = splitAt j (treeMap !! i)
    (top, _:bottom)     = splitAt i (transpose treeMap !! j)
    in t_ij > maximum front
    || t_ij > maximum back
    || t_ij > maximum top
    || t_ij > maximum bottom

------------ PART B ------------
partB :: Input -> OutputB
partB treeMap = maximum
    [ scenicScore (i,j) treeMap
    | i <- [1 .. length treeMap -2]
    , j <- [1 .. length treeMap -2]
    ]

scenicScore :: (Int, Int) -> [[Int]] -> Int
scenicScore (i,j) treeMap = let
    (front, t_ij:back)  = splitAt j (treeMap !! i)
    (top, _:bottom)     = splitAt i (transpose treeMap !! j)

    visibility  = (\(as,bs) -> length as + (fromEnum . not . null) bs) . Data.List.span (< t_ij)
    frontView   = visibility $ reverse front
    backView    = visibility back
    topView     = visibility $ reverse top
    bottomView  = visibility bottom
    in frontView 
    * backView 
    * topView 
    * bottomView