{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Attoparsec.Text as P
import Data.Attoparsec.Text (Parser)
import Data.Text (pack)


main :: IO ()
main = do
  input <- getContents >>= parse
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)


------------ [ Input ] ---------------
type Input = [[Int]]
type Output = Int

------------ [ Parse ] ---------------
parse :: String -> IO Input
parse s =
  case parseInput of
    Left  p ->
      case P.parseOnly p (pack s) of
        Left e  -> error ("Failed while parsing input: " ++ e)
        Right r -> pure r
    Right p -> pure (p s)

parseInput :: Either (Parser Input) (String -> Input)
parseInput = Right $ map (map read . words) . lines


------------ [ Part A ] ---------------
partA :: Input -> Output
partA = sum . map extrapolate

diff :: [Int] -> [Int]
diff xs = zipWith (-) (tail xs) xs

diffs :: [Int] -> [[Int]]
diffs xs
  | all (==0) xs = [xs]
  | otherwise    = xs : diffs (diff xs)

extrapolate :: [Int] -> Int
extrapolate = sum . map last . diffs

------------ [ Part B ] ---------------
partB :: Input -> Output
partB = sum . map extrapolateB

extrapolateB :: [Int] -> Int
extrapolateB = foldr ((-) . head) 0 . diffs
