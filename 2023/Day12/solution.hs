{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Attoparsec.Text as P
import Data.Attoparsec.Text (Parser)
import Data.Text (pack)

import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as M
import Data.Set (Set)
import qualified Data.Set as S
import Data.List (tails, foldl',intercalate)


main :: IO ()
main = do
  input <- getContents >>= parse
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)


------------ [ Input ] ---------------
type Input = [(String, [Int])]

------------ [ Parse ] ---------------
parse :: String -> IO Input
parse s =
  case parseInput of
    Left  p ->
      case P.parseOnly p (pack s) of
        Left e  -> error ("Failed while parsing input: " ++ e)
        Right r -> pure r
    Right p -> pure (p s)

parseInput :: Either (Parser Input) (String -> Input)
parseInput = Left $ parseLine `P.sepBy1` P.endOfLine

parseLine :: Parser (String, [Int])
parseLine = do
  springs <- P.many1 (P.satisfy (P.inClass ".#?"))
  P.space
  groups <- P.decimal `P.sepBy1` P.char ','
  pure (springs, groups)

------------ [ Part A ] ---------------
partA = sum . map (uncurry possibilities)

possibilities conditions gs = snd $ go M.empty conditions wiggleRoom gs
  where
  len = length conditions
  wiggleRoom = len - sum gs - (length gs - 1)
  opts n g = [ replicate i '.' ++ replicate g '#' ++ replicate (n-i) '.' | i <- [0..n] ]

  go :: Map (Int, Int, Int) Int -> String -> Int -> [Int] -> (Map (Int, Int, Int) Int, Int)
  go mem cs n [g] = (mem, length $ filter (equiv cs) (opts n g))
  go mem cs n (g:gs) = foldl' apply (mem, 0) params
    where
    apply (mem', s) (i, cs') = case M.lookup (i, length cs, gslen) mem' of
      Just r -> (mem', s + r)
      Nothing ->
        let (mem'', r) = go mem' cs' (n-i) gs
        in (M.insert (i, length cs, gslen) r mem'', s + r)

    gslen = length gs
    params = [ (i, cs')
      | (i, cs') <- zip [0..n] (drop (g+1) (tails cs))
      , let c = take (length prefix) cs
            prefix = replicate i '.' ++ replicate g '#' ++ "."
      , equiv prefix c
      ]

equiv :: String -> String -> Bool
equiv [] [] = True
equiv _ [] = False
equiv [] _ = False
equiv ('?':as) (_:bs) = equiv as bs
equiv (_:as) ('?':bs) = equiv as bs
equiv (a:as) (b:bs) = a == b && equiv as bs

------------ [ Part B ] ---------------
partB = sum . map (uncurry possibilities . unfold)

unfold (conditions, groups) = (intercalate "?" (replicate 5 conditions), concat (replicate 5 groups))
