import Data.Attoparsec.Text (Parser, parseOnly)
import Data.Text (pack)
import Data.List (groupBy)
import Data.Char (isNumber)


main :: IO ()
main = do
  input <- getContents >>= parse
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)


------------ [ Input ] ---------------
data Symbol = Numb Int Int | Symb Char deriving Show
isSymb (Symb _) = True
isSymb _ = False

type Input = [((Int, Int), Symbol)]
type Output = Int

------------ [ Parse ] ---------------
parse :: String -> IO Input
parse s =
  case parseInput of
    Left  p ->
      case parseOnly p (pack s) of
        Left e  -> error ("Failed while parsing input: " ++ e)
        Right r -> pure r
    Right p -> pure (p s)

parseInput :: Either (Parser Input) (String -> Input)
parseInput = Right
  $ filter (not . isSpace . snd)
  . map collect
  . groupBy (\((ax, ay), a) ((bx, by), b) -> isNumber a && isNumber b && ay == by)
  . concatMap (\(y, l) -> [ ((x, y), c) | (x, c) <- zip [0..] l ]) . zip [0..] . lines
  where
  collect :: [((Int, Int), Char)] -> ((Int, Int), Symbol)
  collect l = ((x, y), symb)
    where
    ((x, y), _) = head l
    value = map snd l
    symb = if isNumber (head value)
      then Numb (length value) (read value)
      else Symb (head value)

  isSpace (Symb '.') = True
  isSpace _ = False




------------ [ Part A ] ---------------
partA :: Input -> Output
partA scheme
  = sum
  . map (toInt . snd)
  . filter (\x -> any ((`isNear` x) . fst) (symbols scheme))
  $ scheme

toInt (Symb _) = 0
toInt (Numb _ i) = i

isNear (x', y') ((x, y), Numb size n) =
  or [ max (abs (x' - x - delta)) (abs (y' - y)) <= 1
      | delta <- [0..(size - 1)]
      ]
isNear syms _ = False

symbols :: Input -> [((Int, Int), Char)]
symbols = map (\(c, Symb s) -> (c, s)) . filter (\(c, s) -> isSymb s)



------------ [ Part B ] ---------------
partB :: Input -> Output
partB scheme
  = sum
  . map (product . map (toInt. snd))
  . filter (\l -> length l == 2)
  . map (\g -> filter (isNear g) scheme)
  $ gears
  where
  gears = map fst (filter isGear (symbols scheme))
  isGear (_, '*') = True
  isGear _ = False
