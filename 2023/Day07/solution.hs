{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (Parser, parseOnly, endOfLine, many1, char, sepBy1, letter, space, decimal, digit)
import Data.Text (pack)
import Control.Applicative ((<|>))
import qualified Data.Map.Strict as M
import Data.List (sortOn, maximumBy)
import Data.Bifunctor
import Data.List.Extra (maximumOn)


main :: IO ()
main = do
  input <- getContents >>= parse
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)


------------ [ Input ] ---------------
type Input = [(String, Int)]
type Output = Int

data Type = H | P1 | P2 | K3 | FH | K4 | K5
  deriving (Show, Eq, Ord)

data Hand
  = Hand Type [Int]
  deriving (Show, Eq, Ord)

hand :: [Int] -> Hand
hand cs
  | len == 1 = Hand K5 cs
  | len == 2 =
      if has 4
      then Hand K4 cs
      else Hand FH cs
  | len == 3 =
      if has 3
      then Hand K3 cs
      else Hand P2 cs
  | len == 4 = Hand P1 cs
  | otherwise = Hand H cs
  where
  fs    = freqs cs
  len   = M.size fs
  has n = M.size (M.filter (==n) fs) == 1

card :: Char -> Int
card '2' = 2
card '3' = 3
card '4' = 4
card '5' = 5
card '6' = 6
card '7' = 7
card '8' = 8
card '9' = 9
card 'T' = 10
card 'J' = 11
card 'Q' = 12
card 'K' = 13
card 'A' = 14

freqs cs = M.fromListWith (+) (map (,1) cs)

------------ [ Parse ] ---------------
parse :: String -> IO Input
parse s =
  case parseInput of
    Left  p ->
      case parseOnly p (pack s) of
        Left e  -> error ("Failed while parsing input: " ++ e)
        Right r -> pure r
    Right p -> pure (p s)

parseInput :: Either (Parser Input) (String -> Input)
parseInput = Left $ parseHand `sepBy1` endOfLine


parseHand :: Parser (String, Int)
parseHand = do
  hand <- many1 (letter <|> digit)
  space
  bid <- decimal
  pure (hand, bid)


------------ [ Part A ] ---------------
partA :: Input -> Output
partA = sum
      . zipWith (*) [1..]
      . map snd
      . sortOn fst
      . map (first (hand . map card))

------------ [ Part B ] ---------------
partB :: Input -> Output
partB = sum
      . zipWith (*) [1..]
      . map snd
      . sortOn fst
      . map (first (hand' . map card'))

card' :: Char -> Int
card' 'J' = 1
card' '2' = 2
card' '3' = 3
card' '4' = 4
card' '5' = 5
card' '6' = 6
card' '7' = 7
card' '8' = 8
card' '9' = 9
card' 'T' = 10
card' 'Q' = 12
card' 'K' = 13
card' 'A' = 14

hand' :: [Int] -> Hand
hand' cs = Hand t cs
  where
  fs = M.delete 1 (freqs cs)
  (max, maxCount) = if M.null fs
    then (1, 5) 
    else maximumOn snd (M.toList fs)

  repl 1 = max
  repl c = c
  (Hand t _) = hand (map repl cs)
