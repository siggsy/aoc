{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Attoparsec.Text as P
import Data.Attoparsec.Text (Parser)
import Data.Text (pack)

import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as M
import Data.Set (Set)
import qualified Data.Set as S
import Data.List (sortOn, foldl', groupBy)


main :: IO ()
main = do
  input <- getContents >>= parse
  putStrLn "Parsed:"
  mapM_ putStrLn input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)


------------ [ Input ] ---------------
type Input = [String]

------------ [ Parse ] ---------------
parse :: String -> IO Input
parse s =
  case parseInput of
    Left  p ->
      case P.parseOnly p (pack s) of
        Left e  -> error ("Failed while parsing input: " ++ e)
        Right r -> pure r
    Right p -> pure (p s)

parseInput :: Either (Parser Input) (String -> Input)
parseInput = Right lines

------------ [ Part A ] ---------------
partA m = load (length m) (rollNorth (mapRocks m))

rollNorth m = M.fromList $ go M.empty (M.toAscList m)
  where
  go :: Map Int Int -> [((Int, Int), Char)] -> [((Int, Int), Char)]
  go _ [] = []
  go highest (((y,x), '#') : rs) = ((y,x), '#') : go (M.insert x y highest) rs
  go highest (((y,x), 'O') : rs) = ((y' + 1, x), 'O') : go (M.insert x (y' + 1) highest) rs
    where
    y' = M.findWithDefault (-1) x highest

load n = sum . map ((n -) . fst) . M.keys . M.filter (== 'O')

enum :: [a] -> [(Int, a)]
enum = zip [0..]

enum2 :: [[a]] -> [((Int, Int), a)]
enum2 as = [ ((y,x), r) | (y, row) <- enum as, (x, r) <- enum row ]

mapRocks :: [String] -> Map (Int, Int) Char
mapRocks m = M.fromAscList [ ((y,x), r) | ((y,x), r) <- enum2 m , r /= '.' ]

------------ [ Part B ] ---------------
partB m = load (length m) (cycle !! r)
  where
  n = 1000000000
  r = mod (n - length offset) (length cycle)
  (offset, cycle) = detectCycle (go n (mapRocks m))
  y = length m - 1
  x = length (head m) - 1
  swap (a,b) = (b,a)
  neg  (a,b) = (y-a, b)
  neg' (a,b) = (x-a, b)

  go n m
    | n == 0 = []
    | m == m' = []
    | otherwise = m : go (n-1) m'
    where
    m' = M.mapKeys (swap . neg')
      . rollNorth . M.mapKeys (swap . neg)
      . rollNorth . M.mapKeys (swap . neg')
      . rollNorth . M.mapKeys (swap . neg)
      $ rollNorth m

detectCycle xs = (before, take (length before) (drop (length before) xs))
  where
  (before, _) = unzip (cycle xs xs)
  cycle (x:xs) (y:yy:ys)
    | x == yy = [(x,yy)]
    | otherwise = (x, yy) : cycle xs ys
