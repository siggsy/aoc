{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (Parser, parseOnly, endOfLine, string, decimal, space, sepBy1, char, many1, letter)
import Data.Text (pack)
import Data.List (foldl')
import Data.Maybe (mapMaybe)


main :: IO ()
main = do
  input <- getContents >>= parse
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)


------------ [ Input ] ---------------
type Input = ([Int], [[((Int, Int), Int)]])
type Output = Int

------------ [ Parse ] ---------------
parse :: String -> IO Input
parse s =
  case parseInput of
    Left  p ->
      case parseOnly p (pack s) of
        Left e  -> error ("Failed while parsing input: " ++ e)
        Right r -> pure r
    Right p -> pure (p s)

parseInput :: Either (Parser Input) (String -> Input)
parseInput = Left $ do
  seeds <- parseSeeds
  endOfLine
  maps <- parseMaps
  pure (seeds, maps)


parseSeeds :: Parser [Int]
parseSeeds = do
  string "seeds: "
  seeds <- decimal `sepBy1` space
  endOfLine
  pure seeds

parseMaps :: Parser [[((Int, Int), Int)]]
parseMaps = parseMap `sepBy1` endOfLine

parseMap :: Parser [((Int, Int), Int)]
parseMap = do
  from <- many1 letter
  string "-to-"
  to <- many1 letter
  string " map:"
  endOfLine
  ranges <- parseRanges `sepBy1` endOfLine
  endOfLine
  pure ranges

parseRanges :: Parser ((Int, Int), Int)
parseRanges = do
  destStart <- decimal
  space
  srcStart <- decimal
  space
  rangeLength <- decimal
  pure ((destStart, srcStart), rangeLength)


------------ [ Part A ] ---------------
partA :: Input -> Output
partA (seeds, maps) = minimum locations
  where
  locations = map (\seed -> foldl' lookupAll seed maps) seeds

lookupAll :: Int -> [((Int, Int), Int)] -> Int
lookupAll s maps = case mapMaybe (lookupMap s) maps of
  (s' : ss) -> s'
  _ -> s

lookupMap :: Int -> ((Int, Int), Int) -> Maybe Int
lookupMap s ((dst, src), range)
  | s >= src && s < src + range = Just (s + dst - src)
  | otherwise = Nothing

------------ [ Part B ] ---------------
partB (seedRange, maps) = minimum locations
  where
  locations = map (fst . fst) seedToLocation
  seedToLocation = foldl' compose (seedToMap seedRange) maps

seedToMap :: [Int] -> [((Int, Int), Int)]
seedToMap [] = []
seedToMap (a : r : ss) = ((a, a), r) : seedToMap ss

compose :: [((Int, Int), Int)] -> [((Int, Int), Int)] -> [((Int, Int), Int)]
compose as bs = filter positiveRange mapped ++ unmapped
  where
    (mapped, unmapped) = foldl' comp ([], as) bs
    comp (res, as) b@(_, brange) = (composits ++ res, concat as')
      where 
        (composits, as') = unzip [ (composit, filter positiveRange rest)
          | a@(_, arange) <- as
          , let rest = [ rangeToMap ((adst, min adst' (bsrc-1)), (asrc, asrc'))
                       , ((right, right + (asrc - adst)), right' - right + 1)
                       ]
                ((adst, adst'), (asrc, asrc')) = mapToRange a
                ((bdst, bdst'), (bsrc, bsrc')) = mapToRange b
                dst      = max adst bsrc + (bdst - bsrc)
                dst'     = min adst' bsrc' + (bdst - bsrc)
                src      = dst + (bsrc - bdst) + (asrc - adst)
                src'     = dst' + (bsrc - bdst) + (asrc - adst)
                composit = rangeToMap ((dst, dst'), (src, src'))
                right    = max adst (bsrc'+1)
                right'   = max adst' bsrc'
          ]
    positiveRange (_,r) = r > 0

mapToRange :: ((Int, Int), Int) -> ((Int, Int), (Int, Int))
mapToRange ((dst, src), size) = ((dst, dst + size - 1), (src, src + size - 1))

rangeToMap :: ((Int, Int), (Int, Int)) -> ((Int, Int), Int)
rangeToMap ((dst, dst'), (src, src')) = ((dst, src), min (dst' - dst) (src' - src) + 1)
