{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (Parser, parseOnly, many1, space, decimal, endOfLine, sepBy1, string, digit, many')
import Data.Text (pack)


main :: IO ()
main = do
  rawInput <- getContents
  input <- parse parseA rawInput
  inputB <- parse (Left parseB) rawInput
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  putStrLn "Parsed:"
  print inputB
  putStrLn "---------------------"
  print (partB inputB)


------------ [ Input ] ---------------
type Input = ([Int], [Int])
type Output = Int

------------ [ Parse ] ---------------
parse :: Either (Parser a) (String -> a) -> String -> IO a
parse parser s =
  case parser of
    Left  p ->
      case parseOnly p (pack s) of
        Left e  -> error ("Failed while parsing input: " ++ e)
        Right r -> pure r
    Right p -> pure (p s)

parseA :: Either (Parser Input) (String -> Input)
parseA = Left $ do
  _ <- string "Time:"
  many1 space
  times <- decimal `sepBy1` many1 space
  endOfLine
  _ <- string "Distance:"
  many1 space
  distances <- decimal `sepBy1` many1 space
  pure (times, distances)


parseB :: Parser (Int, Int)
parseB = do
  _ <- string "Time:"
  many1 space
  time <- digit `sepBy1` many' space
  endOfLine
  _ <- string "Distance:"
  many1 space
  distance <- digit `sepBy1` many' space
  pure (read time, read distance)


------------ [ Part A ] ---------------
partA :: Input -> Output
partA (times, distances) = product $ zipWith beatCount times distances

distance :: Int -> Int -> Int
distance hold time = hold * (time - hold)

options :: Int -> [Int]
options time = [ distance hold time | hold <- [0..time] ]

beatCount :: Int -> Int -> Int
beatCount time record = length (filter (> record) (options time))


------------ [ Part B ] ---------------
partB :: (Int, Int) -> Int
partB (time, record) = halfBeaten * 2 - (if even time then 1 else 0)
  where
  halfBeaten = length $ takeWhile (> record) [ distance hold time | hold <- [time `div` 2 .. time] ]
