{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (Parser)
import Data.Attoparsec.Text qualified as P
import Data.Text (pack)

import Data.Map.Lazy (Map)
import Data.Map.Lazy qualified as M
import Data.Set (Set)
import Data.Set qualified as S

import Data.Array (Array, array, bounds, (!))
import Tools qualified as T

import Debug.Trace

main :: IO ()
main = do
  input <- getContents >>= parse
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)

------------ [ Input ] ---------------
type Input = Array (Int, Int) Char

------------ [ Parse ] ---------------
parse :: String -> IO Input
parse s = pure (parseInput s)

parseInput :: String -> Input
parseInput inp =
  array
    ((0, 0), (length (head ls) - 1, length ls - 1))
    [ ((x, y), c)
    | (y, row) <- zip [0 ..] ls
    , (x, c) <- zip [0 ..] row
    ]
 where
  ls = lines inp

------------ [ Part A ] ---------------
partA = energized . beam ((0, 0), (1, 0))

beam :: ((Int, Int), (Int, Int)) -> Array (Int, Int) Char -> Set ((Int, Int), (Int, Int))
beam start ar = beam' S.empty (S.fromList [start])
 where
  ((lx, ly), (hx, hy)) = bounds ar
  expand front =
    S.unions
      [ case ar ! (x, y) of
        '.' -> increase x y [(dx, dy)]
        '|' -> increase x y [(0, -1), (0, 1)]
        '-' -> increase x y [(-1, 0), (1, 0)]
        '\\' -> increase x y [(dy, dx)]
        '/' -> increase x y [(-dy, -dx)]
      | ((x, y), (dx, dy)) <- S.toList front
      ]
  increase x y dirs =
    S.fromAscList
      [ ((x + dx, y + dy), (dx, dy))
      | (dx, dy) <- dirs
      , let (x', y') = (x + dx, y + dy)
      , x' <= hx && x' >= lx
      , y' <= hy && y' >= ly
      ]

  beam' b front
    | S.null front' = b'
    | otherwise = beam' b' front'
   where
    b' = S.union b front
    front' = expand front S.\\ b'

energized :: Set ((Int, Int), (Int, Int)) -> Int
energized = S.size . S.map fst

------------ [ Part B ] ---------------
partB ar =
  maximum . map energized $
    [beam ((sx, 0), (0, 1)) ar | sx <- [lx .. hx]]
      ++ [beam ((sx, hy), (0, -1)) ar | sx <- [lx .. hx]]
      ++ [beam ((0, sy), (1, 0)) ar | sy <- [ly .. hy]]
      ++ [beam ((hx, sy), (-1, 0)) ar | sy <- [ly .. hy]]
 where
  ((lx, ly), (hx, hy)) = bounds ar
