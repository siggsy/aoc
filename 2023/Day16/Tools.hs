module Tools where

detectCycle :: Eq a => [a] -> ([a], [a])
detectCycle as = cycle as as
  where
  cycle (x:xs) (_:y:ys)
    | x == y    = start as xs
    | otherwise = cycle xs ys
  cycle _ _     = (as, [])

  start (x:xs) (y:ys)
    | x == y    = ([], x : cyc x xs)
    | otherwise = let (xs', ys') = start xs ys in (x:xs', ys')

  cyc x (y:ys)
    | x == y    = []
    | otherwise = y : cyc x ys
