{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Attoparsec.Text as P
import Data.Attoparsec.Text (Parser)
import Data.Text (pack)

import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as M
import Data.Set (Set)
import qualified Data.Set as S
import Data.List (foldl')


main :: IO ()
main = do
  input <- getContents >>= parse
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)


------------ [ Input ] ---------------
type Input = Set (Int, Int)
type Output = Int

------------ [ Parse ] ---------------
parse :: String -> IO Input
parse s =
  case parseInput of
    Left  p ->
      case P.parseOnly p (pack s) of
        Left e  -> error ("Failed while parsing input: " ++ e)
        Right r -> pure r
    Right p -> pure (p s)

parseInput :: Either (Parser Input) (String -> Input)
parseInput = Left $ do 
  image <- P.many1 (P.satisfy (P.inClass ".#")) `P.sepBy1` P.endOfLine
  pure $ S.fromList [ (x,y) | (y, row) <- zip [0..] image, (x, e) <- zip [0..] row, e == '#' ]


------------ [ Part A ] ---------------
partA image = sum (distances expanded)
  where expanded = S.toList (expand 1 image)

distances image = [ abs(x1-x2) + abs(y1-y2) | (x1,y1) <- image, (x2, y2) <- image, (x1,y1) < (x2,y2) ]
 
expand n image = expandedXY
  where
  xs = S.map fst image
  ys = S.map snd image
  (minx, miny) = (minimum xs, minimum ys)
  (maxx, maxy) = (maximum xs, maximum ys)
  columns = [minx .. maxx]
  rows    = [miny .. maxy]
  incrementX c (x,y) = if x > c then (x+n,y) else (x,y)
  incrementY r (x,y) = if y > r then (x,y+n) else (x,y)
  missingColumns = filter (not . flip S.member xs) columns
  missingRows    = filter (not . flip S.member ys) rows
  expandedX = foldl' (\i c -> S.mapMonotonic (incrementX c) i) image (zipWith (+) [0, n ..] missingColumns)
  expandedXY = foldl' (\i r -> S.mapMonotonic (incrementY r) i) expandedX (zipWith (+) [0, n ..] missingRows)

------------ [ Part B ] ---------------
partB image = sum (distances (S.toList (expand 999999 image)))
