{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (Parser)
import Data.Attoparsec.Text qualified as P
import Data.Text (pack)

import Data.Map.Lazy (Map)
import Data.Map.Lazy qualified as M
import Data.Set (Set)
import Data.Set qualified as S

import Tools qualified as T
import Data.List (foldl', sort, partition)
import Data.List.Extra (minimumOn, maximumOn)
import Control.Monad (forM_)

main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="
  
  -- let edge = drawEdge input
  -- draw (S.fromList edge)
  -- putStrLn ""
  -- draw (fill edge)

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)

------------ [ Input ] ---------------
type Input = [Instruction]
type Instruction = (Char, Int, Integer)

------------ [ Parse ] ---------------
runParser :: Parser Input -> String -> IO Input
runParser p s = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

parseInput :: String -> IO Input
parseInput = runParser $ parseInstruction `P.sepBy1` P.endOfLine

parseInstruction :: Parser Instruction
parseInstruction = do
  dir <- P.anyChar
  P.space
  amount <- P.decimal
  P.space
  P.string "(#"
  color <- P.hexadecimal
  P.string ")"
  pure (dir, amount, color)

draw :: Set (Int, Int) -> IO ()
draw m = do
  forM_ [ly..hy] $ \y -> do
    forM_ [lx..hx] $ \x ->
      if S.member (x,y) m
        then putStr "#"
        else putStr "."
    putStrLn ""
  where
  mlist = S.toList m
  (ly, hy) = (minimum $ map snd mlist, maximum $ map snd mlist)
  (lx, hx) = (minimum $ map fst mlist, maximum $ map fst mlist)

------------ [ Part A ] ---------------
partA = S.size . fill . drawEdge

fill :: [(Int, Int)] -> Set (Int, Int)
fill edge = S.union edgeSet (S.fromList [ (x,y) | y <- [ly..hy], x <- inside y ])
  where
  (ly, hy) = (minimum $ map snd edge, maximum $ map snd edge)
  (lx, hx) = (minimum $ map fst edge, maximum $ map fst edge)
  edgeSet = S.fromList edge
  upSet = S.fromList . uncurry (++) $ unzip up
  downSet = S.fromList . uncurry (++) $ unzip down
  (up, down) = partition (\((x1,y1), (x2, y2)) -> y1 < y2)
    . filter (\((x1,y1), (x2,y2)) -> x1 - x2 == 0)
    $ zip edge (tail edge)

  inside y = snd $ foldl' (appendInside y) (False, []) [lx..hx]
  appendInside y (True, ins) x = (not (S.member (x,y) downSet), x : ins)
  appendInside y (False, ins) x = (S.member (x,y) upSet, ins)

drawEdge = foldl' (\edge instr -> move (head edge) instr ++ tail edge) [(0,0)]
  where
  move :: (Int, Int) -> Instruction -> [(Int, Int)]
  move (x,y) (dir, count, _) = [ (x + dx, y + dy) | (dx, dy) <- line' ]
    where
    line = [count,count-1 .. 0]
    line' = case dir of
      'R' -> map (,0) line
      'L' -> map ((,0) . negate) line
      'U' -> map ((0,) . negate) line
      'D' -> map (0,) line

------------ [ Part B ] ---------------
partB = const 0
