{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Attoparsec.Text as P
import Data.Attoparsec.Text (Parser)
import Data.Text (pack)

import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.Set (Set)
import qualified Data.Set as S

import Data.List (find, sort)
import Data.Maybe (fromMaybe)
import Control.Applicative ((<|>))
import Data.List.Extra (minimumOn, maximumOn)


main :: IO ()
main = do
  input <- getContents >>= parse
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)


------------ [ Input ] ---------------
type Input = (Point, Map Point Pipe)
type Output = Int

type Point = (Int, Int)
data Pipe = H | V | NE | NW | SE | SW | S deriving (Show, Eq)
data Edge = E Point Point deriving Show

instance Eq Edge where
  (==) (E a b) (E c d) = (a,b) == (c,d) || (b,a) == (c,d)

instance Ord Edge where
  compare (E a b) (E c d) = compare (sort [a,b]) (sort [c,d])

src :: Edge -> Point
src (E a b) = a

dest :: Edge -> Point
dest (E a b) = b

toPipe :: Char -> Maybe Pipe
toPipe '|' = Just H
toPipe '-' = Just V
toPipe 'L' = Just NE
toPipe 'J' = Just NW
toPipe '7' = Just SW
toPipe 'F' = Just SE
toPipe 'S' = Just S
toPipe _ = Nothing

pipeEdges :: Point -> Pipe -> Set Edge
pipeEdges (x,y) H = S.fromList [E (x,y) (x, y-1), E (x,y) (x,y+1)]
pipeEdges (x,y) V = S.fromList [E (x,y) (x-1, y), E (x,y) (x+1,y)]
pipeEdges (x,y) NE = S.fromList [E (x,y) (x, y-1), E (x,y) (x+1,y)]
pipeEdges (x,y) NW = S.fromList [E (x,y) (x, y-1), E (x,y) (x-1,y)]
pipeEdges (x,y) SW = S.fromList [E (x,y) (x, y+1), E (x,y) (x-1,y)]
pipeEdges (x,y) SE = S.fromList [E (x,y) (x, y+1), E (x,y) (x+1,y)]
pipeEdges (x,y) S = undefined

------------ [ Parse ] ---------------
parse :: String -> IO Input
parse s =
  case parseInput of
    Left  p ->
      case P.parseOnly p (pack s) of
        Left e  -> error ("Failed while parsing input: " ++ e)
        Right r -> pure r
    Right p -> pure (p s)

parseInput :: Either (Parser Input) (String -> Input)
parseInput = Left $ do
  tiles <- P.many1 (P.satisfy (P.inClass "|LJ7FS.") <|> P.char '-')  `P.sepBy1` P.endOfLine
  let
    iTiles = [ ((x,y), t) | (y, row) <- zip [0..] tiles, (x, t) <- zip [0..] row ]
    m = M.mapMaybe toPipe . M.fromList $ iTiles
    sPos = maybe (0,0) fst (find (\(_, t) -> t == 'S') iTiles)
  pure (sPos, m)

------------ [ Part A ] ---------------
partA ((x,y), m) = length (findChain m (x,y)) `div` 2

findChain m (x,y) = go m (S.fromList [E (x,y) a, E (x,y) b]) (a, b)
  where
  neighbs = [ (x+1, y), (x-1,y), (x,y+1), (x,y-1) ]
  (a : b : _) = M.keys
    . M.filter (any ((== (x,y)) . dest) . S.toList)
    . M.mapWithKey pipeEdges
    . M.restrictKeys m
    $ S.fromList neighbs

  go m es (a,b)
    | (a,b) == (a',b') = es
    | otherwise = go m es' (a',b')
    where
    (es', (a',b')) = step m es (a,b)


step :: Map Point Pipe -> Set Edge -> (Point, Point) -> (Set Edge, (Point, Point))
step m es (a, b) = (es', (a', b'))
  where
  edgesA = pipeEdges a (m M.! a)
  edgesB = pipeEdges b (m M.! b)
  es' = S.unions [ es, edgesA, edgesB ]
  freeA = edgesA S.\\ es
  freeB = edgesB S.\\ es
  a' = if S.null freeA then a else dest . head $ S.toList freeA
  b' = if S.null freeB then b else dest . head $ S.toList freeB

------------ [ Part B ] ---------------
partB (start, m) = M.size (M.withoutKeys (M.filter id scan) (S.fromList vertices))
  where
  scan = M.fromList $ concatMap (tail . scanl (\(_,prev) pos -> (pos, isInside prev pos)) (undefined, False)) path
  topLeft = (minimum (map fst vertices), minimum (map snd vertices))
  bottomRight = (maximum (map fst vertices), maximum (map snd vertices))
  vertices = concatMap (\v -> [src v, dest v]) (S.toList chain)
  chain = findChain m start
  path = [ [ (x,y) | x <- [fst topLeft .. fst bottomRight] ] | y <- [snd topLeft .. snd bottomRight] ]

  isInside prev (x,y) = any ((==2) . S.size . S.intersection chain . pipeEdges (x,y)) [NE, NW, H] /= prev

