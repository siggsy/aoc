import Data.Char (isNumber, digitToInt, isDigit)
import Data.List (isPrefixOf, find, stripPrefix, tails)
import Data.Maybe (fromMaybe, mapMaybe)
import Control.Applicative ((<|>))
import Text.ParserCombinators.ReadP (string, ReadP, readP_to_S, many1, satisfy, char, sepBy1, eof)
import Data.Functor (($>))

main :: IO ()
main = do
  input <- map parseLine . lines <$> getContents
  putStrLn "PartA: "
  print (partA input)
  putStrLn "PartB: "
  print (partB input)
  return ()

data Color = Red | Green | Blue deriving Show
type Reveal = (Int, Color)

firstParse :: ReadP a -> String -> a
firstParse p = fst . head . readP_to_S p

intP :: ReadP Int
intP = do
  digits <- many1 $ satisfy isDigit
  pure $ read digits

parseLine :: String -> (Int, [[Reveal]])
parseLine = firstParse $ do
  string "Game "
  gameId <- intP
  string ": "
  reveals <- (parseReveal `sepBy1` string ", ") `sepBy1` string "; "
  eof
  pure (gameId, reveals)

parseReveal :: ReadP Reveal
parseReveal = do
  count <- intP
  char ' '
  color <- (string "blue" $> Blue) <|> (string "red" $> Red) <|> (string "green" $> Green)
  pure (count, color)

revealToPair :: [Reveal] -> (Int, Int, Int)
revealToPair =
  foldl
    (\(r, g, b) (count, color) -> case color of
      Red -> (r+count, g, b)
      Blue -> (r, g, b+count)
      Green -> (r, g+count, b))
    (0,0,0)

isPossible :: (Int, Int, Int) -> Bool
isPossible (r,g,b) = r <= 12 && g <= 13 && b <= 14

possibleGame :: (Int, [[Reveal]]) -> Bool
possibleGame (gameId, reveals) = all (isPossible . revealToPair) reveals

partA :: [(Int, [[Reveal]])] -> Int
partA = sum . map fst . filter possibleGame




maxPossible :: (Int, [[Reveal]]) -> (Int, Int, Int)
maxPossible (gameId, reveals)
  = foldl1
    (\(r,g,b) (r',g',b') -> (max r r', max g g', max b b'))
    (map revealToPair reveals)

partB :: [(Int, [[Reveal]])] -> Int
partB = sum . map ((\(r,g,b) -> r*g*b) . maxPossible)
