import Data.Char (isNumber, digitToInt)
import Data.List (isPrefixOf, find, stripPrefix, tails)
import Data.Maybe (fromMaybe, mapMaybe)
import Control.Applicative ((<|>))

main :: IO ()
main = do
  input <- lines <$> getContents
  putStrLn "PartA: "
  print (partA input)
  putStrLn "PartB: "
  print (partB input)
  return ()

firstAndLast :: [Int] -> Int
firstAndLast [] = 0
firstAndLast [n] = 10 * n + n
firstAndLast ns = 10 * head ns + last ns



readNumbers :: String -> [Int]
readNumbers = map digitToInt . filter isNumber

partA :: [String] -> Int
partA = sum . map (firstAndLast . readNumbers)



readNumbers' :: String -> [Int]
readNumbers' = mapMaybe toDigit . tails
  where
  wordNums = 
    [ "one"
    , "two"
    , "three"
    , "four"
    , "five"
    , "six"
    , "seven"
    , "eight"
    , "nine"
    ]
  toDigit [] = Nothing
  toDigit sss@(s:ss) = 
    let wordNum = snd <$> find ((`isPrefixOf` sss) . fst) (zip wordNums [1..])
        digit   = if isNumber s then Just (digitToInt s) else Nothing
    in wordNum <|> digit

partB :: [String] -> Int
partB = sum . map (firstAndLast . readNumbers')
