{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-deferred-out-of-scope-variables #-}

import qualified Data.Attoparsec.Text as P
import Data.Attoparsec.Text (Parser)
import Data.Text (pack)

import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as M
import Data.Set (Set)
import qualified Data.Set as S
import Data.List (foldl')

import qualified Tools as T
import Data.Char (ord, isLetter)
import Data.Array (Array, array, (//), accum)
import Data.Maybe (fromMaybe)


main :: IO ()
main = do
  input <- getContents >>= parse
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)


------------ [ Input ] ---------------
type Input = [String]

------------ [ Parse ] ---------------
parse :: String -> IO Input
parse s =
  case parseInput of
    Left  p ->
      case P.parseOnly p (pack s) of
        Left e  -> error ("Failed while parsing input: " ++ e)
        Right r -> pure r
    Right p -> pure (p s)

parseInput :: Either (Parser Input) (String -> Input)
parseInput = Left $ P.many1 (P.satisfy (\c -> c /= ',' && c /= '\n')) `P.sepBy1` P.char ','


------------ [ Part A ] ---------------
partA = sum . map hash

hash :: String -> Int
hash = foldl' (\s c -> ((s+c) * 17) `mod` 256) 0 . map ord

------------ [ Part B ] ---------------
partB = focusingPower
      . foldl' (\b f -> f b) initBoxes
      . map toOperation

type Boxes = Map Int [(String, Int)]

initBoxes :: Boxes
initBoxes = M.empty

insertLens :: String -> Int -> Boxes -> Boxes
insertLens label focal = M.alter (Just . ins (label, focal) . fromMaybe []) (hash label)
  where
  ins new [] = [new]
  ins new@(newLabel, _) (old@(oldLabel, _):olds)
    | newLabel == oldLabel = new : olds
    | otherwise            = old : ins new olds

deleteLens :: String -> Boxes -> Boxes
deleteLens label = M.alter (Just . del label . fromMaybe []) (hash label)
  where
  del ll [] = []
  del ll (l'@(ll', _):ls)
    | ll == ll'  = ls
    | otherwise = l' : del ll ls

toOperation :: String -> (Boxes -> Boxes)
toOperation op = case f of
  '-' -> deleteLens label
  '=' -> insertLens label (read args)
  where
  (label, f : args) = span isLetter op

focusingPower :: Boxes -> Int
focusingPower = sum
              . map (\(b, ls) -> sum
                   . map ((b+1) *)
                   . zipWith (*) [1..]
                   $ map snd ls)
              . M.toList
