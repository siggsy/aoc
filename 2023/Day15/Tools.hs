module Tools where

detectCycle :: Eq a => [a] -> ([a], [a])
detectCycle as = cycle as as
  where
  cycle (x:xs) (_:y:ys)
    | x == y    = cyc as xs
    | otherwise = cycle xs ys
  cycle _ _     = (as, [])

  cyc (x:xs) (y:ys)
    | x == y    = ([], x : rest x xs)
    | otherwise = let (xs', ys') = cyc xs ys in (x:xs', ys')

  rest x (y:ys)
    | x == y    = []
    | otherwise = y : rest x ys
