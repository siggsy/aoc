{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Attoparsec.Text as P
import Data.Attoparsec.Text (Parser)
import Data.Text (pack)

import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as M
import Data.Set (Set)
import qualified Data.Set as S
import Control.Applicative ((<|>))
import Data.List (transpose)


main :: IO ()
main = do
  input <- getContents >>= parse
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)


------------ [ Input ] ---------------
type Input = [[String]]

------------ [ Parse ] ---------------
parse :: String -> IO Input
parse s =
  case parseInput of
    Left  p ->
      case P.parseOnly p (pack s) of
        Left e  -> error ("Failed while parsing input: " ++ e)
        Right r -> pure r
    Right p -> pure (p s)

parseInput :: Either (Parser Input) (String -> Input)
parseInput = Left $ parseMap `P.sepBy1` P.endOfLine


parseMap :: Parser [String]
parseMap = P.many1 (P.many1 (P.char '.' <|> P.char '#') <* P.endOfLine)

------------ [ Part A ] ---------------
partA maps = 100 * sum rows + sum columns
  where
  columns = map (findMirror . transpose) maps
  rows = map findMirror maps

findMirror :: [String] -> Int
findMirror m
  = headOrZero
  . filter (`isMirror` m)
  . map fst
  . filter (uncurry (==) . snd)
  $ zip [1..] (zip m (tail m))
  where
  headOrZero [] = 0
  headOrZero (x:_) = x

isMirror :: Int -> [String] -> Bool
isMirror n m = left == right
  where
  (a, a') = splitAt n m
  (left, right) = if n > length m `div` 2
    then (take (length a') (reverse a), a')
    else (reverse a, take (length a) a')


------------ [ Part B ] ---------------
partB maps = 100 * sum rows + sum columns
  where
  columns = map (findMirrorB . transpose) maps
  rows = map findMirrorB maps


diff [] [] = 0
diff as [] = length as
diff [] bs = length bs
diff (a:as) (b:bs)
  | a == b = diff as bs
  | otherwise = 1 + diff as bs

findMirrorB :: [String] -> Int
findMirrorB m
  = headOrZero
  . map fst
  . filter (uncurry (isMirrorB m))
  . filter ((<= 1) . snd)
  $ zip [1..] (zipWith diff m (tail m))
  where
  headOrZero [] = 0
  headOrZero (x:_) = x

isMirrorB m n c = c + diffs == 1
  where
  diffs = sum (zipWith diff (drop 1 left) (drop 1 right))
  (a, a') = splitAt n m
  (left, right) = if n > length m `div` 2
    then (take (length a') (reverse a), a')
    else (reverse a, take (length a) a')

