{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Attoparsec.Text as P
import Data.Attoparsec.Text (Parser)
import Data.Text (pack)
import Control.Applicative
import Data.Functor (($>))

import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.List (transpose)


main :: IO ()
main = do
  input <- getContents >>= parse
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)


------------ [ Input ] ---------------
type Input = ([Direction], Network)
type Output = Int
data Direction = L | R deriving (Show, Eq)
type Network = Map String (String, String)

------------ [ Parse ] ---------------
parse :: String -> IO Input
parse s =
  case parseInput of
    Left  p ->
      case P.parseOnly p (pack s) of
        Left e  -> error ("Failed while parsing input: " ++ e)
        Right r -> pure r
    Right p -> pure (p s)

parseInput :: Either (Parser Input) (String -> Input)
parseInput = Left $ do
  directions <- readDirections
  P.endOfLine
  nodes <- readNode `P.sepBy1` P.endOfLine
  pure (directions, M.fromList nodes)

readDirections :: Parser [Direction]
readDirections = P.many1 ((P.char 'L' $> L) <|> (P.char 'R' $> R)) <* P.endOfLine

readNode :: Parser (String, (String, String))
readNode = do
  name <- readName
  P.string " = ("
  left <- readName
  P.string ", "
  right <- readName
  P.string ")"
  pure (name, (left, right))

readName :: Parser String
readName = P.count 3 (P.letter <|> P.digit)

------------ [ Part A ] ---------------
partA (directions, network) = length . takeWhile (/= "ZZZ") $ traverseNetwork network directions "AAA"
  where movement = cycle directions

move :: Network -> Direction -> String -> String
move net L pos = fst (net M.! pos)
move net R pos = snd (net M.! pos)

traverseNetwork network directions start = scanl (flip (move network)) start (cycle directions)

------------ [ Part B ] ---------------
partB (directions, network) = foldr1 lcm (map offsetLen cycles)
  where
  offsetLen (off, cyc) = length off
  cycles = map (detectCycle . zip (cycle [0..length directions-1]) . traverseNetwork network directions) as
  as = filter ((=='A') . last) (M.keys network)


detectCycle xs = (before, take (length before) (drop (length before) xs))
  where
  (before, _) = unzip (cycle xs xs)
  cycle (x:xs) (y:yy:ys)
    | x == yy = [(x,yy)]
    | otherwise = (x, yy) : cycle xs ys
