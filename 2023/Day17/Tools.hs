module Tools where
import qualified Data.Set as S
import Data.Set (Set)

import qualified Data.Map.Lazy as M
import Data.Map.Lazy (Map)

detectCycle :: Eq a => [a] -> ([a], [a])
detectCycle as = cycle as as
  where
  cycle (x:xs) (_:y:ys)
    | x == y    = start as xs
    | otherwise = cycle xs ys
  cycle _ _     = (as, [])

  start (x:xs) (y:ys)
    | x == y    = ([], x : cyc x xs)
    | otherwise = let (xs', ys') = start xs ys in (x:xs', ys')

  cyc x (y:ys)
    | x == y    = []
    | otherwise = y : cyc x ys


dijkstra :: (Ord a, Ord b)
  => ((a, b) -> [(a, b)])
  -> Set b -> (a, b)
  -> Maybe [(a,b)]
dijkstra neighbs ends start = search S.empty (M.singleton start [start]) where
  search visited next = case M.minViewWithKey next of
    Just (((cost, vertex), path), next')
      | vertex `S.member` ends    -> Just path
      | vertex `S.member` visited -> search visited next'
      | otherwise                 -> search (S.insert vertex visited) next''
      where next'' = foldr (\n m -> M.insertWith (++) n [n] m) next' (neighbs (cost, vertex))
    _ -> Nothing

dijkstra' :: (Ord a, Ord b)
  => ((a, b) -> [(a, b)])
  -> Set b -> (a, b)
  -> Maybe (a,b)
dijkstra' neighbs ends start = head <$> dijkstra neighbs ends start
