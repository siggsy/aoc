{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (Parser)
import Data.Attoparsec.Text qualified as P
import Data.Text (pack)

import Data.Map.Lazy (Map)
import Data.Map.Lazy qualified as M
import Data.Set (Set)
import Data.Set qualified as S

import Tools qualified as T
import Data.Array (Array, array, bounds, (!))
import Data.Char (digitToInt)
import Tools (dijkstra, dijkstra')
import Data.Maybe (fromMaybe)

main :: IO ()
main = do
  input <- getContents >>= parseInput
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)

------------ [ Input ] ---------------
type Input = Array (Int, Int) Int
type Direction = ((Int, Int), Int)
type Position = (Int, Int)

------------ [ Parse ] ---------------
runParser :: String -> Parser Input -> IO Input
runParser s p = case P.parseOnly p (pack s) of
  Left e -> error ("Failed while parsing input: " ++ e)
  Right r -> pure r

parseInput :: String -> IO Input
parseInput inp = pure $
  array
    ((0,0), (length (head ls) - 1, length ls - 1))
    [ ((x,y), digitToInt heat)
      | (y, row) <- zip [0..] ls
      , (x, heat) <- zip [0..] row
      ]
  where
  ls = lines inp

------------ [ Part A ] ---------------
partA ar
  = fst . fromMaybe (0,((0,0), ((0,0),0)))
  $ dijkstra'
      (neighbours ar)
      (S.fromList
        [ ((hx, hy), ((dx, dy), c)) | (dx, dy) <- [(0,-1), (0,1), (-1,0), (1,0)], c <- [1..3] ])
      (0, ((0,0), ((0,0),0)))
  where ((lx, ly), (hx, hy)) = bounds ar

neighbours :: Array Position Int -> (Int, (Position, Direction)) -> [(Int, (Position, Direction))]
neighbours ar (heatLoss, ((x,y), (direction, count))) =
  [ (heatLoss', ((x',y'), (direction', count')))
    | (dx, dy) <- case direction of  
        (0,0) -> [(0,1), (1,0), (0,-1), (-1,0)]
        (1,0) -> [(1,0), (0,1), (0,-1)]
        (0,1) -> [(0,1), (1,0), (-1,0)]
        (-1,0) -> [(-1,0), (0,1), (0,-1)]
        (0,-1) -> [(0,-1), (1,0), (-1,0)]

    , let (x', y') = (x + dx, y + dy)
          ((lx, ly), (hx, hy)) = bounds ar

    , x' <= hx && x' >= lx
    , y' <= hy && y' >= ly

    , let heatLoss' = heatLoss + ar ! (x', y')
          direction' = (dx, dy)
          count' = if direction == (dx, dy) then count + 1 else 1
    , count' <= 3
    ]

------------ [ Part B ] ---------------
partB ar
  = fst . fromMaybe (0,((0,0), ((0,0),0)))
  $ dijkstra'
      (neighboursB ar)
      (S.fromList
        [ ((hx, hy), ((dx, dy), c)) | (dx, dy) <- [(0,-1), (0,1), (-1,0), (1,0)], c <- [4..10] ])
      (0, ((0,0), ((0,0),0)))
  where ((lx, ly), (hx, hy)) = bounds ar
--
neighboursB :: Array Position Int -> (Int, (Position, Direction)) -> [(Int, (Position, Direction))]
neighboursB ar (heatLoss, ((x,y), (direction, count))) =
  [ (heatLoss', ((x',y'), (direction', count')))
    | (dx, dy) <- if count < 4 && direction /= (0,0) then [direction] else case direction of  
        (0,0) -> [(0,1), (1,0), (0,-1), (-1,0)]
        (1,0) -> [(1,0), (0,1), (0,-1)]
        (0,1) -> [(0,1), (1,0), (-1,0)]
        (-1,0) -> [(-1,0), (0,1), (0,-1)]
        (0,-1) -> [(0,-1), (1,0), (-1,0)]

    , let (x', y') = (x + dx, y + dy)
          ((lx, ly), (hx, hy)) = bounds ar

    , x' <= hx && x' >= lx
    , y' <= hy && y' >= ly

    , let heatLoss' = heatLoss + ar ! (x', y')
          direction' = (dx, dy)
          count' = if direction == (dx, dy) then count + 1 else 1
    , count' <= 10
    ]
--
