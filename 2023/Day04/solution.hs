{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (Parser, parseOnly, number, decimal, string, char, count, digit, space, sepBy1, endOfLine, endOfInput, anyChar, inClass, satisfy, (<?>), many1)
import Data.Text (pack)
import Data.List (intersect)
import Control.Applicative ((<|>))


main :: IO ()
main = do
  input <- getContents >>= parse
  putStrLn "Parsed:"
  print input
  putStrLn "==============================="

  putStrLn "Part A: "
  print (partA input)
  putStrLn ""

  putStrLn "Part B: "
  print (partB input)


------------ [ Input ] ---------------
type Input = [([Int], [Int])]
type Output = Int

------------ [ Parse ] ---------------
parse :: String -> IO Input
parse s =
  case parseInput of
    Left  p ->
      case parseOnly p (pack s) of
        Left e  -> error ("Failed while parsing input: " ++ e)
        Right r -> pure r
    Right p -> pure (p s)

parseInput :: Either (Parser Input) (String -> Input)
parseInput = Left $ parseCard `sepBy1` endOfLine

parseCard :: Parser ([Int], [Int])
parseCard = do
  string "Card"
  many1 space
  _ <- decimal
  string ": "
  winning <- num `sepBy1` char ' '
  string " | "
  my <- num `sepBy1` char ' '
  pure (winning, my)

num :: Parser Int
num = read <$> count 2 (satisfy (inClass " 0-9"))


------------ [ Part A ] ---------------
partA :: Input -> Output
partA = sum . map (points . winningCount)
  where
    points c =
      if c == 0
        then 0
        else 2^(c-1)

winningCount :: ([Int], [Int]) -> Int
winningCount (winning, my) = length (winning `intersect` my)


------------ [ Part B ] ---------------
partB :: Input -> Output
partB = sum . copyCards . zip (repeat 1) . map winningCount
  where
  copyCards [] = []
  copyCards ((n, 0) : cs) = n : copyCards cs
  copyCards ((n, w) : cs) = n : copyCards (map (\(n', w') -> (n' + n, w')) toCopy ++ rest)
    where (toCopy, rest) = splitAt w cs
